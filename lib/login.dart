import 'package:flutter/material.dart';
import 'package:libertas6/Pages/Home.dart';
// import 'Animation/FadeAnimation.dart';
// nuevas librerias
import 'Constant.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Login(),
    // routes: {
    //   '/login': (context) => Login(),
    //   '/profile': (context) => Profile(),
    //   '/forgotPassword': (context) => ForgotPassword(),
    // },
    // initialRoute: '/login',
  ));
}

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final user = TextEditingController();
  final pass = TextEditingController();
  String _usuario = '';
  String _password = '';

  String _textModal =
      '''Necesita identificarse para brindarle un mejor servicio e normas actualizadas''';
  String _textTitleModal = 'Sugerencia';
  // TextEditingController user;
  // TextEditingController pass;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // backgroundColor: Constant.colorLogoBack,
      body: Container(
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // _getBackGroundd(), // PARTE SUPERIOR DEL LOGIN
              this._getTitleIniciarSesion(),
              Padding(
                padding: EdgeInsets.only(right: 30.0, left: 30, top: 50),
                child: Column(
                  children: <Widget>[
                    _getUsernameAndPasword(),
                    SizedBox(
                      height: 30,
                    ),
                    _getButtomIngresar(),
                    SizedBox(
                      height: 5,
                    ),
                    _getButtomIngresarSinCuenta(),
                    SizedBox(
                      height: 30,
                    ),
                    _getButtomContraOlvidada(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  // Widget _getBackGroundd() {
  //   return Container(
  //     height: 450,
  //     color: Constant.colorLogoBack,
  //     child: Stack(
  //       children: <Widget>[
  //         Positioned(
  //           child: Container(
  //             // color: Colors.black,
  //             margin: EdgeInsets.only(top: 50),
  //             child: Center(
  //               child: Image(
  //                 image: AssetImage('assets/images/logo.jpg'),
  //               ),
  //             ),
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }

  Widget _getTitleIniciarSesion() {
    return Container(
      child: Text(
        'Iniciar Sesion',
        style: TextStyle(
          fontSize: 50,
          color: Constant.colorLogoBack,
          // decoration: TextDecoration.underline,
        ),
      ),
    );
  }

  Widget _getUsernameAndPasword() {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            // color: Color.fromRGBO(143, 148, 251, 2),
            color: Constant.colorLogoBack,
            blurRadius: 20.0, // BRILLANTE AL CUADRO
            offset: Offset(0, 10), // MUEVE LA SOMBRA X-Y
          )
        ],
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.grey[100]),
              ),
            ),
            child: TextField(
              controller: user,
              autofocus: false,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Username',
                hintStyle: TextStyle(color: Colors.grey[400]),
              ),
              // onChanged: (text) {
              //   print("se cambio el nombre de user: $text");
              //   // print('repetido : ${user.text}');
              //   user.text = text;
              // },
            ),
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              controller: pass,
              autofocus: false,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Password',
                hintStyle: TextStyle(color: Colors.grey[400]),
              ),
              // onChanged: (text) {
              //   print("valor de contraseña: $text");
              //   // print('repetido : ${user.text}');
              //   pass.text = text;
              // },
              obscureText: true,
            ),
          ),
        ],
      ),
    );
  }

  Widget _getButtomIngresar() {
    return Container(
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Constant.colorLogoBack,
        // gradient: LinearGradient(colors: [
        //   Color.fromRGBO(143, 148, 251, 1),
        //   Color.fromRGBO(143, 148, 251, 0.6),
        // ]),
      ),
      child: InkWell(
        // button
        child: Center(
          child: Text(
            'Ingresar',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
        onTap: () {
          _usuario = user.text;
          _password = pass.text;
          print('boton presionado ' + _usuario + _password);
          if (user.text == 'admin' && pass.text == 'admin') {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Home()),
            );
          } else {
            print('no entra');
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Home()),
            );
          }
        },
      ),
    );
  }

  Widget _getButtomIngresarSinCuenta() {
    return Container(
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Constant.colorLogoBack,
        // gradient: LinearGradient(colors: [
        //   Color.fromRGBO(143, 148, 251, 1),
        //   Color.fromRGBO(143, 148, 251, 0.6),
        // ]),
      ),
      child: InkWell(
        // button
        child: Center(
          child: Text(
            'Ingresar sin cuenta',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
        onTap: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(child: Text(_textTitleModal)),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Center(
                        child: Text(_textModal),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  TextButton(
                    child: Text(
                      'Registrarme',
                      style: TextStyle(color: Constant.colorLogoBack),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      print('Boton de registrarse presionado');
                    },
                  ),
                  TextButton(
                    child: Text(
                      'Continuar sin cuenta',
                      style: TextStyle(
                          color: Constant.colorLogoBack, fontSize: 18),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                      );
                    },
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }

  Widget _getButtomContraOlvidada() {
    return InkWell(
      child: Text(
        '¿Contraseña olvidada?',
        style: TextStyle(
          // color: Color.fromRGBO(143, 148, 251, 1),
          color: Constant.primaryColor,
          fontSize: 16,
        ),
      ),
      onTap: () {
        print('presionando boton de contraseña olvidada');
      },
    );
  }
}
