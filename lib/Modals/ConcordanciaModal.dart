import 'package:flutter/material.dart';

import '../Constant.dart';

class ConcordanciaModal extends ModalRoute<void> {
  List<Widget> _elements;

  ConcordanciaModal(List<Widget> elements) {
    this._elements = elements;
  }

  @override
  Duration get transitionDuration => Duration(milliseconds: 0);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.5);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: SafeArea(
        child: _buildOverlayContent(context),
      ),
    );
  }

  // arma el modal con respecto al plugin para que no se oculte el menu principal
  Widget _buildOverlayContent(BuildContext context) {
    Widget p = new Row(children: [
      new Container(
          margin: EdgeInsets.only(right: 10),
          child: new IconButton(
            icon: new Icon(Icons.arrow_back),
            color: Constant.primaryColor,
            onPressed: () {
              Navigator.pop(context);
            },
          )),
      new Text(
        'CONCORDANCIA',
        style: TextStyle(color: Color(0xFF965565), fontWeight: FontWeight.bold),
      )
    ]);

    List<Widget> elements = List<Widget>.from(this._elements);
    elements.insert(0, p);

    return Container(
        margin: EdgeInsets.only(left: 40, right: 40, bottom: 120, top: 120),
        padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
        color: Colors.white,
        child: Column(children: elements));
  }
}
