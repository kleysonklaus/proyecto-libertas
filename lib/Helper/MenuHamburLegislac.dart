import 'package:flutter/material.dart';
// import 'package:libertas6/Helper/componentesMenuH/AcercaLibertas.dart';
import 'package:libertas6/Helper/componentesMenuH/BuscarArticuloporN.dart';
import 'package:libertas6/Helper/componentesMenuH/CalificarApp.dart';
// import 'package:libertas6/Helper/componentesMenuH/Colaboracion.dart';
import 'package:libertas6/Helper/componentesMenuH/CondicionesYPoliticas.dart';
// import 'package:libertas6/Helper/componentesMenuH/Cuenta.dart';
import 'package:libertas6/Helper/componentesMenuH/ReportarProblemaApp.dart';
import 'package:libertas6/Helper/componentesMenuH/ServicioAyuda.dart';
import 'package:libertas6/Helper/componentesMenuH/CompartirApp.dart';
import 'package:libertas6/Helper/componentesMenuH/VisitarWebLibertas.dart';

class HamburguesaLegislacion extends StatefulWidget {
  final String numNorma;

  HamburguesaLegislacion({Key key, this.numNorma = ''}) : super(key: key);

  @override
  _HamburguesaLegislacionState createState() => _HamburguesaLegislacionState();
}

class _HamburguesaLegislacionState extends State<HamburguesaLegislacion> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: popupMenuButtonProfile2(widget.numNorma),
    );
  }

  // menu hamburguesa para cuando este solo en legislacion
  Widget popupMenuButtonProfile2(String norma) {
    final double apu = 500; //anchoPopUp
    final double alpu = 50; //altoPopUp
    final double mt = 1; //margenTodo
    final double sep = 20; //separacion

    return PopupMenuButton(
      icon: new Icon(Icons.menu, color: Colors.white),
      color: Colors.grey[50],
      itemBuilder: (context) => [
        PopupMenuItem(
            child: BuscarArticuloporN(
                apu: apu, alpu: alpu, mt: mt, sep: sep, norma: norma)),
        PopupMenuItem(
            child: ServicioAyuda(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child: ReportarProblemaApp(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child:
                CondicionesYPoliticas(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        // PopupMenuItem(
        //     child: AcercaLibertas(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        // PopupMenuItem(child: Cuenta(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        // PopupMenuItem(
        //     child: Colaboracion(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child: CalificarApp(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child: CompartirApp(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child: VisitarWebLibertas(apu: apu, alpu: alpu, mt: mt, sep: sep))
      ],
    );
  }
}
