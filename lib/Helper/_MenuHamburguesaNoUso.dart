import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/componentesMenuH/_ReportProblem.dart';

abstract class Hamburguesa {
  static Widget popupMenuButtonProfile() {
    final double apu = 500; //anchoPopUp
    final double alpu = 50; //altoPopUp
    final double mt = 1; //margenTodo
    final double sep = 20; //separacion
    return PopupMenuButton(
      icon: new Icon(Icons.menu, color: Colors.white),
      color: Colors.grey[50],
      itemBuilder: (context) => [
        PopupMenuItem(child: getServicioDeAyuda(apu, alpu, mt, sep)),
        PopupMenuItem(child: getReportarProblema(apu, alpu, mt, sep, context)),
        PopupMenuItem(child: getCondicionesYPoliticas(apu, alpu, mt, sep)),
        PopupMenuItem(child: getAcercaDeLibertas(apu, alpu, mt, sep)),
        PopupMenuItem(child: getCuenta(apu, alpu, mt, sep)),
        PopupMenuItem(child: getColaboracion(apu, alpu, mt, sep)),
        PopupMenuItem(child: getCalificar(apu, alpu, mt, sep)),
        PopupMenuItem(child: getCompartir(apu, alpu, mt, sep)),
        PopupMenuItem(child: getVisitarWebLibertas(apu, alpu, mt, sep, context))
      ],
    );
  }

  static Widget getServicioDeAyuda(
      double apu, double alpu, double mt, double sep) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(
            Icons.border_outer_sharp,
            color: Constant.colorIconsPopUp,
          ),
          SizedBox(width: sep),
          Text(
            'Servicio de Ayuda',
            style: TextStyle(
                color: Constant.colorIconsPopUp, fontWeight: FontWeight.bold),
          )
        ]));
  }

  static Widget getReportarProblema(
      double apu, double alpu, double mt, double sep, BuildContext context) {
    return Container(
      margin: EdgeInsets.all(mt),
      width: apu,
      height: alpu,
      child: InkWell(
        child: Row(
          children: [
            Icon(
              Icons.notifications_active_outlined,
              color: Constant.colorIconsPopUp,
            ),
            SizedBox(width: sep),
            Text(
              'Reportar Problema',
              style: TextStyle(color: Constant.colorIconsPopUp),
            )
          ],
        ),
        onTap: () {
          //IMPORTANTE => CIERRA EL BACK LIST EN 1 MODAL
          Navigator.pop(context);
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return alertDialogReportProblem(context);
            },
          );
        },
      ),
    );
  }

  static Widget getCondicionesYPoliticas(
      double apu, double alpu, double mt, double sep) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(
            Icons.autorenew_sharp,
            color: Constant.colorIconsPopUp,
          ),
          SizedBox(width: sep),
          Text(
            'Condiciones y Políticas',
            style: TextStyle(color: Constant.colorIconsPopUp),
          )
        ]));
  }

  static Widget getAcercaDeLibertas(
      double apu, double alpu, double mt, double sep) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(
            Icons.bookmark_border,
            color: Constant.colorIconsPopUp,
          ),
          SizedBox(width: sep),
          Text(
            'Acerca de LIBERTAS',
            style: TextStyle(color: Constant.colorIconsPopUp),
          )
        ]));
  }

  static Widget getCuenta(double apu, double alpu, double mt, double sep) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(
            Icons.person_outline,
            color: Constant.colorIconsPopUp,
          ),
          SizedBox(width: sep),
          Text(
            'Cuenta',
            style: TextStyle(color: Constant.colorIconsPopUp),
          )
        ]));
  }

  static Widget getColaboracion(
      double apu, double alpu, double mt, double sep) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(
            Icons.credit_card,
            color: Constant.colorIconsPopUp,
          ),
          SizedBox(width: sep),
          Text(
            'Colaboración',
            style: TextStyle(color: Constant.colorIconsPopUp),
          )
        ]));
  }

  static Widget getCalificar(double apu, double alpu, double mt, double sep) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(
            Icons.local_police_outlined,
            // Icons.star_border_outlined,
            color: Constant.colorIconsPopUp,
          ),
          SizedBox(width: sep),
          Text(
            'Calificar',
            style: TextStyle(color: Constant.colorIconsPopUp),
          )
        ]));
  }

  static Widget getCompartir(double apu, double alpu, double mt, double sep) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(
            Icons.settings_outlined,
            color: Constant.colorIconsPopUp,
          ),
          SizedBox(width: sep),
          Text(
            'Compartir',
            style: TextStyle(color: Constant.colorIconsPopUp),
          )
        ]));
  }

  static Widget getVisitarWebLibertas(
      double apu, double alpu, double mt, double sep, BuildContext context) {
    return Container(
      margin: EdgeInsets.all(mt),
      width: apu,
      height: alpu,
      child: InkWell(
        child: Row(
          children: [
            Icon(
              Icons.border_clear,
              color: Constant.colorIconsPopUp,
            ),
            SizedBox(width: sep),
            Text(
              'Visitar web LIBERTAS',
              style: TextStyle(color: Constant.colorIconsPopUp),
            ),
            SizedBox(width: sep),
            Icon(Icons.exit_to_app, color: Constant.colorIconsPopUp)
          ],
        ),
        onTap: () {
          //IMPORTANTE => CIERRA EL BACK LIST EN 1 MODAL
          print("visitando!");
          Navigator.pop(context);
          // showDialog(
          //   context: context,
          //   builder: (BuildContext context) {
          //     return _launchURL(context);
          //   },
          // );
        },
      ),
    );
  }

  //$$$$$$$$$$$$$$$$$$$$$$$$FUNCIONES DE ITEMS$$$$$$$$$$$$$$$

  static Widget alertDialogReportProblem(BuildContext context) {
    return AlertDialog(
      elevation: 30,
      title: Text(
        'REPORTAR UN PROBLEMA',
        style: TextStyle(color: Constant.primaryColor),
      ),
      content: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          height: 350,
          width: 350,
          child:
              ReportProblemApp(), //CLASES DE LOS CHIPS DE PROBLEMA ReportProblem.dart
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Cancelar',
            style: TextStyle(color: Constant.colorLogoBack),
          ),
          onPressed: () {
            Navigator.pop(context);
            print('Boton de cancelar presionado');
          },
        ),
        TextButton(
          child: Text(
            'Enviar',
            style: TextStyle(color: Constant.colorLogoBack),
          ),
          onPressed: () {
            print('Boton de enviar presionado');
          },
        ),
      ],
    );
  }
}
