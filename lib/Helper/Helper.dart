import 'package:libertas6/Classes/MyChip.dart';
import 'package:libertas6/Constant.dart';

abstract class Helper {
  static List<String> getSelectedsOfChips(String tipo) {
    List<String> urls = new List();

    List<MyChip> chips = new List();
    if (tipo == "legislacion") {
      chips = Constant.legislacionChips;
    } else if (tipo == "titulo") {
      chips = Constant.legislacionChips;
    }

    chips.forEach((element) {
      if (element.isSelected()) {
        urls.add(element.getUrl());
      }
    });

    return urls;
  }

  static String getMesNombre(int indice) {
    List<String> meses = [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Diciembre'
    ];
    return meses[indice];
  }

  static String getDiaSemanaNombre(int indice) {
    List<String> dias = [
      'Lunes',
      'Martes',
      'Miercoles',
      'Jueves',
      'Viernes',
      'Sabado',
      'Domingo'
    ];
    return dias[indice];
  }

  // obtiene el mes en nombre
  static String nameMonth(String f) {
    List dat = f.split("-");
    return getMesNombre(int.parse(dat[1]) - 1);
  }

  // obtiene el mes en numero
  static int numMonth(String f) {
    List dat = f.split("-");
    return int.parse(dat[1]);
  }

  // obtiene el año en numero
  static int numYear(String f) {
    List dat = f.split("-");
    return int.parse(dat[0]);
  }

  // obtiene el dia en numero
  static int numDay(String f) {
    List dat = f.split("-");
    return int.parse(dat[2]);
  }
}
