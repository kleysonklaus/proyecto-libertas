import 'package:flutter/material.dart';
import 'package:libertas6/Helper/MenuHamburLegislac.dart';
import 'package:libertas6/Helper/MenuHamburGeneral.dart';
import 'package:libertas6/Modals/ChipsModal.dart';
import 'package:libertas6/Pages/Inicio/LegislacionResultados.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../Constant.dart';

// import 'MenuHamburguesa.dart';

abstract class WidgetHelper {
  static Widget getLogoTopAnimated(BuildContext context) {
    return Expanded(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Constant.colorLogoBack,
            expandedHeight: 70,
            floating: false,
            pinned: false,
            title: Row(
              children: [
                Container(
                  child: new Image.asset(
                    'assets/images/logo.jpg',
                    width: 120.0,
                    fit: BoxFit.cover,
                  ),
                ),
                Expanded(child: Container()),
                PopupMenuButton<String>(
                  icon: new Icon(
                    Icons.menu,
                    color: Colors.white,
                  ),
                  itemBuilder: (BuildContext context) {
                    return {'Logout', 'Settings'}.map(
                      (String choice) {
                        return PopupMenuItem<String>(
                          value: choice,
                          child: Text(choice),
                        );
                      },
                    ).toList();
                  },
                ),
              ],
            ),
            // flexibleSpace: FlexibleSpaceBar(
            //   background: Container(color: Colors.black),
            // ),
          ),
          SliverFillRemaining(
            child: Container(
              color: Colors.white,
              child: Text('data'),
            ),
          ),
        ],
      ),
    );
  }

  // obtiene el app bar con el menu hamburguesa
  static Widget _getMainWith(Widget container) {
    return Scaffold(
      // body: CustomScrollView(
      //   slivers: <Widget>[
      //     SliverAppBar(
      //       backgroundColor: Constant.colorLogoBack,
      //       expandedHeight: 70,
      //       floating: false,
      //       pinned: false,
      //       title: Row(
      //         children: [
      //           Container(
      //             child: new Image.asset(
      //               'assets/images/logo.jpg',
      //               width: 120.0,
      //               fit: BoxFit.cover,
      //             ),
      //           ),
      //           Expanded(child: Container()), //separacion de iconos
      //           popupMenuButtonProfile(),
      //         ],
      //       ),
      //     ),
      //     SliverFillRemaining(
      //       // hasScrollBody: false,
      //       // fillOverscroll: false,

      //       child: container,
      //     ),
      //   ],
      // ),
      body: SafeArea(
        child: Container(padding: EdgeInsets.all(0), child: container),
      ),
    );
  }

  // obtiene el appbar con el menu hamburguesa de perfil
  static Widget getAppMenuTopProfile(
      {bool cambio = false, String urlTemp = ''}) {
    return Container(
      color: Constant.colorLogoBack,
      height: 70,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 15),
            child: new Image.asset(
              'assets/images/logo.jpg',
              width: 120.0,
              fit: BoxFit.cover,
            ),
          ),
          Expanded(child: Container()), //separacion de iconos
          cambio == false
              // ? Hamburguesa.popupMenuButtonProfile() //menu hamburguesa
              ? HamburguesaGeneral()
              : HamburguesaLegislacion(numNorma: urlTemp),
        ],
      ),
    );
  }

  // obtiene el logo de la app
  static Widget getLogoTop(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width * 1.0,
      // COLOR PARA EL LOGO TOP RGB
      // color: Color.fromRGBO(4, 20, 79, 1),
      color: Constant.colorLogoBack,
      alignment: Alignment.center,
      child: new Image.asset('assets/images/logo.jpg', width: 120.0),
    );
  }

  static Widget getMainContainerWithAppBar(List<Widget> elements) {
    return _getMainWith(new Column(children: elements));
    // return _getMainWith(new ListView(children: elements));
  }

  // static Widget getMainContainerWithoutAppBar(List<Widget> elements) {
  //   return _getMainWithout(new Column(children: elements));
  // }

  // static Widget getDynamicContainer(List<Widget> elements) {
  //   return _getMainWith(new SingleChildScrollView(
  //       child: new Column(
  //     children: elements,
  //   )));

  //   //return _getMain(new ListView(children: elements,));
  // }

  // obtiene el widget de loading
  static Widget getWidgetLoader() {
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  // muestra una alerta
  static void showAlert(BuildContext context, Widget title, Widget content) {
    AlertDialog dialog = new AlertDialog(
      title: title,
      content: content,
    );
    _showDialog(context, dialog);
  }

  static void _showDialog(context, dialog) {
    showDialog(
      context: context,
      builder: (context) {
        return dialog;
      },
    );
  }
  /* End Dialog */

  // arma el buscador principal
  static Widget getBuscador(String typeOfChip, BuildContext context) {
    TextEditingController controller = new TextEditingController();

    return new Container(
      color: Color(0xFFF4F3FB),
      margin: EdgeInsets.only(
          left: Constant.containerMarginLeft,
          right: Constant.containerMarginRight),
      child: new Row(
        children: [
          new Container(
            margin: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: new Icon(Icons.search),
              onPressed: () {
                //print(controller.value.text);
                pushNewScreen(
                  context,
                  screen: LegislacionResultados(palabraBuscar: controller.text),
                  withNavBar: true, // OPTIONAL VALUE. True by default.
                  pageTransitionAnimation: PageTransitionAnimation.cupertino,
                );
              },
            ),
          ),
          new Expanded(
            child: new TextField(
              controller: controller,
              decoration: new InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                hintText: 'Buscar',
              ),
            ),
          ),
          new Container(
            margin: EdgeInsets.only(left: 10),
            child: new IconButton(
              icon: new Icon(Icons.dehaze, color: Constant.primaryColor),
              onPressed: () {
                pushDynamicScreen(
                  context,
                  screen: ChipsModal("legislacion"),
                  withNavBar: false,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
