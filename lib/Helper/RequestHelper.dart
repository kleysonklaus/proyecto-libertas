import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../Constant.dart';

import 'dart:collection';

abstract class RequestHelper {
  static HashMap _data = new HashMap<String, dynamic>();

  static String _errorMessage = 'Fallo cargando los datos';

  static Future<List<dynamic>> getListFromConfig(String filename) async {
    final contents = await rootBundle.loadString('assets/contents/' + filename);
    return json.decode(contents);
  }

  static Future<Map<String, dynamic>> getObjectFromAPI(String url) async {
    // http.Response response = await http.get(Constant.apiUrl + url);
    String urlConsulta = Constant.apiUrl + url;
    print("EL PERUANOOOOOOOOOOOOOOOOOOOOOO");
    print(urlConsulta);

    if (!RequestHelper._data.containsKey(urlConsulta)) {
      http.Response response = await http.get(urlConsulta);
      if (response.statusCode == 200 && response.body != null) {
        RequestHelper._data[urlConsulta] = response.body;
        // print(RequestHelper._data[urlConsulta]);
      } else {
        throw new Exception(_errorMessage);
      }
    }
    return json.decode(RequestHelper._data[urlConsulta]);
  }

  // static Future<Map<String, dynamic>> getObjectFromAPI(String url) async {
  //   http.Response response = await http.get(Constant.apiUrl + url);
  //   print(Constant.apiUrl + url);
  //   // String source = Utf8Decoder().convert(response.bodyBytes);
  //   String source = latin1.decode(response.bodyBytes);
  //   print(source);
  //   if (response.statusCode == 200) {
  //     print("EL PERUANOOOOOOOOOOOOOOOOOOOOOO");
  //     // return json.decode(response.body);
  //     return json.decode(source);
  //   } else {
  //     throw new Exception(_errorMessage);
  //   }
  // }

  static Future<List<dynamic>> getListFromAPI(String url) async {
    String urlConsulta = Constant.apiUrl + url;
    print("BUSCANDO ... ");
    // http.Response response = await http.get(urlConsulta);
    print(urlConsulta);

    if (!RequestHelper._data.containsKey(urlConsulta)) {
      http.Response response = await http.get(urlConsulta);
      if (response.statusCode == 200 && response.body != null) {
        RequestHelper._data[urlConsulta] = response.body;
        // print(RequestHelper._data[urlConsulta]);
      } else {
        throw new Exception(_errorMessage);
      }
    }
    return json.decode(RequestHelper._data[urlConsulta]);
  }

  static Future<List<dynamic>> getListFromAPIjuris(String url) async {
    String urlConsulta = Constant.apiUrlJuris + url;
    print("BUSCANDO ... ");
    // http.Response response = await http.get(urlConsulta);
    print(urlConsulta);

    if (!RequestHelper._data.containsKey(urlConsulta)) {
      http.Response response = await http.get(urlConsulta);
      if (response.statusCode == 200 && response.body != null) {
        RequestHelper._data[urlConsulta] = response.body;
        // print(RequestHelper._data[urlConsulta]);
      } else {
        throw new Exception(_errorMessage);
      }
    }
    return json.decode(RequestHelper._data[urlConsulta]);
  }

  // static Future<List<dynamic>> getListFromAPI(String url) async {
  //   String urlConsulta = Constant.apiUrl + url;
  //   print("BUSCANDO ... ");
  //   http.Response response = await http.get(urlConsulta);
  //   print(urlConsulta);
  //   if (response.statusCode == 200) {
  //     return json.decode(response.body);
  //   } else {
  //     throw new Exception(_errorMessage);
  //   }
  // }

  static Future<List<dynamic>> getSearchArticleAPI(
      String norma, String arti) async {
    print("BUSCANDO ARTICULO... ");
    String urlConsulta = Constant.apiUrl +
        '/get_articulo' +
        '/' +
        arti.toUpperCase() +
        '/' +
        norma;
    print(urlConsulta);

    if (!RequestHelper._data.containsKey(urlConsulta)) {
      http.Response response = await http.get(urlConsulta);
      if (response.statusCode == 200 && response.body != null) {
        RequestHelper._data[urlConsulta] = response.body;
        // print(RequestHelper._data[urlConsulta]);
      } else {
        throw new Exception(_errorMessage);
      }
    }
    return json.decode(RequestHelper._data[urlConsulta]);
  }

  // static Future<List<dynamic>> getSearchArticleAPI(
  //     String norma, String arti) async {
  //   print("BUSCANDO ARTICULO... ");
  //   http.Response response = await http.get(Constant.apiUrl +
  //       '/get_articulo' +
  //       '/' +
  //       arti.toUpperCase() +
  //       '/' +
  //       norma);
  //   print(Constant.apiUrl + '/get_articulo' + '/' + arti + '/' + norma);
  //   if (response.statusCode == 200) {
  //     return json.decode(response.body);
  //   } else {
  //     throw new Exception(_errorMessage);
  //   }
  // }

  static String utf8convert(String text) {
    List<int> bytes = text.toString().codeUnits;
    return utf8.decode(bytes);
  }
}
