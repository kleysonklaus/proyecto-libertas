import 'package:flutter/material.dart';
// import 'package:libertas6/Helper/componentesMenuH/AcercaLibertas.dart';
import 'package:libertas6/Helper/componentesMenuH/CalificarApp.dart';
// import 'package:libertas6/Helper/componentesMenuH/Colaboracion.dart';
import 'package:libertas6/Helper/componentesMenuH/CondicionesYPoliticas.dart';
// import 'package:libertas6/Helper/componentesMenuH/Cuenta.dart';
import 'package:libertas6/Helper/componentesMenuH/ReportarProblemaApp.dart';
import 'package:libertas6/Helper/componentesMenuH/ServicioAyuda.dart';
import 'package:libertas6/Helper/componentesMenuH/CompartirApp.dart';
import 'package:libertas6/Helper/componentesMenuH/VisitarWebLibertas.dart';

class HamburguesaGeneral extends StatefulWidget {
  HamburguesaGeneral({Key key}) : super(key: key);

  @override
  _HamburguesaGeneralState createState() => _HamburguesaGeneralState();
}

class _HamburguesaGeneralState extends State<HamburguesaGeneral> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: popupMenuButtonProfile(),
    );
  }

  // arma el menu hamburguesa del INICIO, perteneciente al menu principal
  Widget popupMenuButtonProfile() {
    final double apu = 500; //anchoPopUp
    final double alpu = 50; //altoPopUp
    final double mt = 1; //margenTodo
    final double sep = 20; //separacion
    return PopupMenuButton(
      icon: new Icon(Icons.menu, color: Colors.white),
      color: Colors.grey[50],
      itemBuilder: (context) => [
        PopupMenuItem(
            child: ServicioAyuda(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child: ReportarProblemaApp(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child:
                CondicionesYPoliticas(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        // PopupMenuItem(
        //     child: AcercaLibertas(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        // PopupMenuItem(child: Cuenta(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        // PopupMenuItem(
        //     child: Colaboracion(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child: CalificarApp(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child: CompartirApp(apu: apu, alpu: alpu, mt: mt, sep: sep)),
        PopupMenuItem(
            child: VisitarWebLibertas(apu: apu, alpu: alpu, mt: mt, sep: sep))
      ],
    );
  }
}
