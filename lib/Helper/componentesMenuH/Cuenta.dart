import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';

class Cuenta extends StatelessWidget {
  final double apu; //anchoPopUp
  final double alpu; //altoPopUp
  final double mt; //margenTodo
  final double sep; //separacion

  const Cuenta({Key key, this.apu, this.alpu, this.mt, this.sep})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(Icons.person_outline, color: Constant.colorIconsPopUp),
          SizedBox(width: sep),
          Text('Cuenta', style: TextStyle(color: Constant.colorIconsPopUp))
        ]));
  }
}
