import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:launch_review/launch_review.dart';

class CalificarApp extends StatelessWidget {
  final double apu; //anchoPopUp
  final double alpu; //altoPopUp
  final double mt; //margenTodo
  final double sep; //separacion

  const CalificarApp({Key key, this.apu, this.alpu, this.mt, this.sep})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: InkWell(
            child: Row(children: [
              Icon(
                Icons.local_police_outlined,
                // Icons.star_border_outlined,
                color: Constant.colorIconsPopUp,
              ),
              SizedBox(width: sep),
              Text('Calificar',
                  style: TextStyle(color: Constant.colorIconsPopUp))
            ]),
            onTap: () {
              print("calificando app!");
              Navigator.pop(context);
              this.calificar();
            }));
  }

  void calificar() {
    LaunchReview.launch(
      androidAppId: "com.gruposistemas.libertas",
      // iOSAppId: "585027354",
      iOSAppId: "com.gruposistemas.libertas",
    );
  }

  // Widget getCalificar(double apu, double alpu, double mt, double sep) {
  //   return Container(
  //     margin: EdgeInsets.all(mt),
  //     width: apu,
  //     height: alpu,
  //     child: Row(
  //       children: [
  //         Icon(
  //           Icons.local_police_outlined,
  //           // Icons.star_border_outlined,
  //           color: Constant.colorIconsPopUp,
  //         ),
  //         SizedBox(width: sep),
  //         Text(
  //           'Calificar',
  //           style: TextStyle(color: Constant.colorIconsPopUp),
  //         )
  //       ],
  //     ),
  //   );
  // }
}
