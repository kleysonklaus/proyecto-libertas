import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/componentesMenuH/_modalBuscArti.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class BuscarArticuloporN extends StatelessWidget {
  final double apu; //anchoPopUp
  final double alpu; //altoPopUp
  final double mt; //margenTodo
  final double sep; //separacion
  final String norma;

  const BuscarArticuloporN(
      {Key key, this.apu, this.alpu, this.mt, this.sep, this.norma})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(mt),
      width: apu,
      height: alpu,
      child: InkWell(
        child: Row(children: [
          Icon(Icons.border_outer_sharp, color: Constant.colorIconsPopUp),
          SizedBox(width: sep),
          Text('Buscar artículos por #',
              style: TextStyle(
                  color: Constant.colorIconsPopUp, fontWeight: FontWeight.bold))
        ]),
        onTap: () {
          print("BUSCAR EN: " + norma);
          Navigator.pop(context);
          pushDynamicScreen(context,
              screen: BuscarArticulo(norma), withNavBar: true);
        },
      ),
    );
  }
  // Widget _getBuscarArticulo(double apu, double alpu, double mt, double sep,
  //     String norma, BuildContext context) {
  //   return Container(
  //     margin: EdgeInsets.all(mt),
  //     width: apu,
  //     height: alpu,
  //     child: InkWell(
  //       child: Row(
  //         children: [
  //           Icon(
  //             Icons.border_outer_sharp,
  //             color: Constant.colorIconsPopUp,
  //           ),
  //           SizedBox(width: sep),
  //           Text(
  //             'Buscar artículos por #',
  //             style: TextStyle(
  //                 color: Constant.colorIconsPopUp, fontWeight: FontWeight.bold),
  //           )
  //         ],
  //       ),
  //       onTap: () {
  //         print("BUSCAR EN: " + norma);
  //         Navigator.pop(context);
  //         pushDynamicScreen(
  //           context,
  //           screen: BuscarArticulo(norma),
  //           withNavBar: true,
  //         );
  //       },
  //     ),
  //   );
  // }

}
