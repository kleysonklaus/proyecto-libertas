import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/componentesMenuH/_ResultBusqArti.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class BuscarArticulo extends ModalRoute<void> {
  String _numNorma;
  TextEditingController _busArt = TextEditingController();
  BuscarArticulo(String norma) {
    this._numNorma = norma;
  }

  @override
  Duration get transitionDuration => Duration(milliseconds: 0);
  @override
  bool get opaque => false;
  @override
  bool get barrierDismissible => false;
  @override
  Color get barrierColor => Colors.black.withOpacity(0.5);
  @override
  String get barrierLabel => null;
  @override
  bool get maintainState => true;
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: SafeArea(
        child: Column(
          children: [
            _buildOverlayContent(context),
          ],
        ),
      ),
    );
  }

  Widget _buildOverlayContent(BuildContext context) {
    Widget itemsTop = Row(children: [
      Container(
          margin: EdgeInsets.only(right: 10),
          child: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Constant.primaryColor,
            onPressed: () {
              Navigator.pop(context);
            },
          )),
      Text('Escriba el # del articulo',
          style: TextStyle(
            color: Constant.colorLogoBack,
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ))
    ]);

    // List<Widget> elements = List<Widget>.from(this._elements);
    List<Widget> elements = new List();
    elements.add(itemsTop);
    elements.add(
      TextFormField(
        controller: _busArt,
        textAlign: TextAlign.center,
        cursorColor: Colors.black,
        keyboardType: TextInputType.number,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        // keyboardType: inputType,
        decoration: new InputDecoration(
            // border: InputBorder.none,
            // focusedBorder: InputBorder.none,
            // enabledBorder: InputBorder.none,
            // errorBorder: InputBorder.none,
            // disabledBorder: InputBorder.none,
            contentPadding:
                EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15)),
      ),
    );
    elements.add(Container(
        padding: EdgeInsets.only(top: 7),
        width: 140,
        child: FlatButton(
            color: Colors.orange,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18),
              side: BorderSide(color: Colors.orange),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.search, color: Colors.white),
                Text('BUSCAR',
                    style: TextStyle(color: Colors.white, fontSize: 20))
              ],
            ),
            onPressed: () {
              print('MODAL num norma -> ' + _numNorma);
              print('palabra -> ' + _busArt.value.text.toUpperCase());
              // // RequestHelper.getSearchArticleAPI(url,_busArt.value.text.toUpperCase());
              Navigator.of(context).pop();
              // this._setPostsArticle(url , _busArt.value.text.toUpperCase());

              pushNewScreen(
                context,
                screen: ResultadoArticulo(
                    norma: _numNorma, palabra: _busArt.value.text),
                withNavBar: true,
                pageTransitionAnimation: PageTransitionAnimation.cupertino,
              );
            })));

    return Container(
        margin: EdgeInsets.only(left: 40, right: 40, bottom: 120, top: 120),
        padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
        color: Colors.white,
        child: Column(children: elements));
  }
}
