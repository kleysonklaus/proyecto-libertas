import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:url_launcher/url_launcher.dart';

class VisitarWebLibertas extends StatelessWidget {
  final double apu; //anchoPopUp
  final double alpu; //altoPopUp
  final double mt; //margenTodo
  final double sep; //separacion

  const VisitarWebLibertas({Key key, this.apu, this.alpu, this.mt, this.sep})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(mt),
      width: apu,
      height: alpu,
      child: InkWell(
        child: Row(
          children: [
            Icon(Icons.border_clear, color: Constant.colorIconsPopUp),
            SizedBox(width: sep),
            Text('Visitar web LIBERTAS',
                style: TextStyle(color: Constant.colorIconsPopUp))
          ],
        ),
        onTap: () {
          print("visitando Web Libertas!");
          Navigator.pop(context);
          _launchURL();
        },
      ),
    );
  }

  _launchURL() async {
    const url = 'https://libertas.pe/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
