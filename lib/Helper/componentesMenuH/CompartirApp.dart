// import 'package:flutter/material.dart';
// import 'package:flutter_share_me/flutter_share_me.dart';
// import 'package:libertas6/Constant.dart';

// class CompartirApp extends StatelessWidget {
//   final double apu; //anchoPopUp
//   final double alpu; //altoPopUp
//   final double mt; //margenTodo
//   final double sep; //separacion
//   final String msg = '''
// Hey! Prueba esta nueva app sobre las nuevas leyes del Perú
// En Android:
// https://play.google.com/store/apps/details?id=com.gruposistemas.libertas
// ''';
//   const CompartirApp({Key key, this.apu, this.alpu, this.mt, this.sep})
//       : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsets.all(mt),
//       width: apu,
//       height: alpu,
//       child: InkWell(
//         child: Row(children: [
//           Icon(
//             Icons.share,
//             color: Constant.colorIconsPopUp,
//           ),
//           SizedBox(width: sep),
//           Text(
//             'Compartir',
//             style: TextStyle(color: Constant.colorIconsPopUp),
//           )
//         ]),
//         onTap: () async {
//           Navigator.pop(context);
//           print("compartido! en Systema");
//           var response = await FlutterShareMe().shareToSystem(msg: msg);
//           if (response == 'success') {
//             print('navigate success');
//           }
//           print(response);
//         },
//       ),
//     );
//   }
// }
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:libertas6/Constant.dart';

class CompartirApp extends StatefulWidget {
  final double apu; //anchoPopUp
  final double alpu; //altoPopUp
  final double mt; //margenTodo
  final double sep; //separacion
  CompartirApp({this.apu, this.alpu, this.mt, this.sep});

  @override
  _CompartirAppState createState() => _CompartirAppState();
}

class _CompartirAppState extends State<CompartirApp> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(widget.mt),
      width: widget.apu,
      height: widget.alpu,
      child: InkWell(
        child: Row(children: [
          Icon(Icons.share, color: Constant.colorIconsPopUp),
          SizedBox(width: widget.sep),
          Text('Compartir', style: TextStyle(color: Constant.colorIconsPopUp))
        ]),
        onTap: () async {
          Navigator.pop(context);
          print("compartido! en Sistema");
          await _shareText();
        },
      ),
    );
  }

  Future<void> _shareText() async {
    final String msg = '''
Hey! Prueba esta nueva app sobre las nuevas leyes del Perú
En Android:
https://play.google.com/store/apps/details?id=com.gruposistemas.libertas
''';
    try {
      Share.text('Libertas App', msg, 'text/plain');
    } catch (e) {
      print('error: $e');
    }
  }
}

//  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// import 'package:flutter/material.dart';
// import 'package:libertas6/Constant.dart';

// import 'package:share_extend/share_extend.dart';

// class CompartirApp extends StatefulWidget {
//   final double apu; //anchoPopUp
//   final double alpu; //altoPopUp
//   final double mt; //margenTodo
//   final double sep; //separacion
//   final String msg = '''
// Hey! Prueba esta nueva app sobre las nuevas leyes del Perú
// En Android:
// https://play.google.com/store/apps/details?id=com.gruposistemas.libertas
// ''';

//   CompartirApp({this.apu, this.alpu, this.mt, this.sep});
//   @override
//   _CompartirAppState createState() => _CompartirAppState();
// }

// class _CompartirAppState extends State<CompartirApp> {
//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsets.all(widget.mt),
//       width: widget.apu,
//       height: widget.alpu,
//       child: InkWell(
//         child: Row(children: [
//           Icon(
//             Icons.share,
//             color: Constant.colorIconsPopUp,
//           ),
//           SizedBox(width: widget.sep),
//           Text(
//             'Compartir',
//             style: TextStyle(color: Constant.colorIconsPopUp),
//           )
//         ]),
//         onTap: () async {
//           Navigator.pop(context);
//           print("compartido! en Sistema");
//           ShareExtend.share(
//             widget.msg,
//             "text",
//             sharePanelTitle: "Libertas App",
//             subject: "Libertas App",
//           );
//         },
//       ),
//     );
//   }
// }
