import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Classes/ChipsReportProblems.dart';
import 'package:flutter/material.dart';

// ======================================
import 'dart:async';
import 'dart:io';

import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:image_picker/image_picker.dart';

class ReportProblemApp extends StatefulWidget {
  ReportProblemApp({Key key}) : super(key: key);

  @override
  _ReportProblemAppState createState() => _ReportProblemAppState();
}

class _ReportProblemAppState extends State<ReportProblemApp> {
  List<ChipProblem> myChips = new List();
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(child: _getLocalReportChips());
  }

  @override
  void initState() {
    super.initState();

    myChips = Constant.problemChips;
    print(myChips.length);
    this.setChipsNoSelected(myChips);
    for (var item in myChips) {
      print(item.getLabel() + '->' + item.isSelected().toString());
    }
  }

  Widget _getLocalReportChips() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: Wrap(
            direction: Axis.vertical,
            spacing: 3.0,
            runSpacing: 1.0,
            children: myChips
                .map(
                  (chip) => Row(
                    children: <Widget>[
                      Checkbox(
                        materialTapTargetSize: MaterialTapTargetSize.values[1],
                        activeColor: Constant.colorLogoBack,
                        value: chip.isSelected() ? true : false,
                        onChanged: (bool value) {
                          setState(
                            () {
                              chip.setSelected(value);
                            },
                          );
                          // this.verifyAllChhips(myChips);
                        },
                      ),
                      InkWell(
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          // width: 200,
                          child: Text(
                            chip.getLabel(),
                            // style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        onTap: () {
                          setState(
                            () {
                              chip.setSelected(!chip.isSelected());
                            },
                          );
                        },
                      ),
                      // SizedBox(width: 5),
                    ],
                  ),
                )
                .toList(),
          ),
        ),
        Text(
          'Especificar:',
          // style: TextStyle(fontWeight: FontWeight.bold),
        ),
        TextFormField(
          controller: _controller,
          enabled:
              this.myChips[this.myChips.length - 1].isSelected() ? true : false,
          autofocus: false,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Constant.colorLogoBack, width: 3.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 2.0),
            ),
          ),
        ),
        Row(
          children: [
            Expanded(
              child: RaisedButton(
                child: Text('Cancelar'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: RaisedButton(
                child: Text('Enviar'),
                onPressed: () {
                  // verifyAllChhips(myChips);
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SendMailGmail(
                            opciones: myChips, textOtro: _controller.text)),
                  );

                  // Navigator.of(context).push(
                  //   MaterialPageRoute(
                  //       builder: (context) => SendMailGmail(
                  //           opciones: myChips, textOtro: _controller.text)),
                  // );
                },
              ),
            ),
          ],
        )
      ],
    );
  }

  void verifyAllChhips(List myChips) {
    print('############# - Estados actuales - #############');
    for (var i in this.myChips) {
      print(i.getLabel() + ' => ' + i.isSelected().toString());
    }
  }

  void setChipsNoSelected(List myChips) {
    for (var i in this.myChips) {
      i.setSelected(false);
    }
  }
}

// ===========================================================================

class SendMailGmail extends StatefulWidget {
  final List<ChipProblem> opciones;
  final String textOtro;

  SendMailGmail({this.opciones, this.textOtro});

  @override
  _SendMailGmailState createState() => _SendMailGmailState();
}

class _SendMailGmailState extends State<SendMailGmail> {
  List<String> attachments = [];
  bool isHTML = false;
  final _sopEmailController =
      TextEditingController(text: 'kleyson@gruposistemas.com');
  final _asuntoController =
      TextEditingController(text: 'REPORTAR PROBLEMA CON LA APP');
  // TextEditingController _asuntoController = TextEditingController();

  final _bodyEmailController = TextEditingController(text: '');
  // TextEditingController _bodyEmailController = TextEditingController();

  @override
  void initState() {
    super.initState();
    String tempB = '';
    for (var chip in widget.opciones) {
      if (chip.isSelected()) {
        tempB += chip.getLabel() + '\n';
      }
    }
    tempB += widget.textOtro;
    _bodyEmailController.text = tempB;
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> send() async {
    final Email email = Email(
      body: _bodyEmailController.text,
      subject: _asuntoController.text,
      recipients: [_sopEmailController.text],
      attachmentPaths: attachments,
      isHTML: isHTML,
    );

    String platformResponse;

    try {
      await FlutterEmailSender.send(email);
      platformResponse = 'Acción ejecutada exitosamente';
    } catch (error) {
      platformResponse = error.toString();
    }

    if (!mounted) return;

    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(platformResponse),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return
        // MaterialApp(
        // debugShowCheckedModeBanner: false,
        // theme: ThemeData(primaryColor: Constant.colorLogoBack),
        // home:
        Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Reportar Problema'),
        actions: <Widget>[
          IconButton(
            onPressed: send,
            // icon: Icon(Icons.send),
            icon: Icon(FontAwesomeIcons.solidPaperPlane, size: 20),
          )
        ],
      ),
      body: this._getContenido(),
    );
    // );
  }

  void _openImagePicker() async {
    File pick = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (pick != null) {
      setState(() {
        attachments.add(pick.path);
      });
    }
  }

  void _removeAttachment(int index) {
    setState(() {
      attachments.removeAt(index);
    });
  }

  Widget _getContenido() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              enabled: false,
              controller: _sopEmailController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Soporte',
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              controller: _asuntoController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Asunto',
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: TextField(
                controller: _bodyEmailController,
                maxLines: null,
                expands: true,
                textAlignVertical: TextAlignVertical.top,
                decoration: InputDecoration(
                    labelText: 'Email', border: OutlineInputBorder()),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                for (var i = 0; i < attachments.length; i++)
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          attachments[i],
                          softWrap: false,
                          overflow: TextOverflow.fade,
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.remove_circle),
                        onPressed: () => {_removeAttachment(i)},
                      )
                    ],
                  ),
                TextButton(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text('Seleccionar Imagenes',
                          style: TextStyle(
                              fontSize: 15, color: Constant.colorLogoBack)),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.image, color: Constant.colorLogoBack),
                      ),
                    ],
                  ),
                  onPressed: _openImagePicker,
                  // onPressed: getImage,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
