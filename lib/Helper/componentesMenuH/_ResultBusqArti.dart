import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';

class ResultadoArticulo extends StatefulWidget {
  final String norma;
  final String palabra;
  ResultadoArticulo({Key key, this.norma, this.palabra}) : super(key: key);

  @override
  _ResultadoArticuloState createState() => _ResultadoArticuloState();
}

class _ResultadoArticuloState extends State<ResultadoArticulo> {
  List<dynamic> _postsArticle = new List();
  bool cargando = true;
  bool existe = false;
  @override
  void initState() {
    super.initState();
    _setPostsArticle(widget.norma, widget.palabra);
  }

  void _setPostsArticle(String numNorma, String palabra) async {
    RequestHelper.getSearchArticleAPI(numNorma, palabra).then((response) {
      setState(() {
        this._postsArticle = response;
        // print('resultado:');
        // print(_postsArticle);
        cargando = false;
        if (_postsArticle.isNotEmpty) {
          existe = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getAppMenuTopProfile(cambio: true, urlTemp: widget.norma),
      _getContenido(),
    ];

    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  Widget _getContenido() {
    return cargando
        ? WidgetHelper.getWidgetLoader()
        : Expanded(
            child: ListView(
              shrinkWrap: true,
              children: [
                mostrarJsonArticulo(),
              ],
            ),
          );
    // : Container(
    //   alignment: Alignment.center,
    //   height: MediaQuery.of(context).size.height - 164,
    //   padding: EdgeInsets.all(20),
    //   child: mostrarJsonArticulo(),
    // );
  }

  bool _verificarArt(String art, String titulo) {
    if (titulo.contains(art))
      return true;
    else
      return false;
  }

  Widget mostrarJsonArticulo() {
    return !existe
        ? Text('No Existe ...')
        : Column(
            children: this._postsArticle.map((e) {
              return Container(
                margin: EdgeInsets.only(top: 10, bottom: 10, left: 8, right: 8),
                decoration: !this._verificarArt(widget.palabra, e['Titulo'])
                    ? BoxDecoration()
                    : BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25),
                            bottomLeft: Radius.circular(25),
                            bottomRight: Radius.circular(25)),
                        boxShadow: [
                          BoxShadow(
                            color: Constant.colorLogoBack.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                padding: EdgeInsets.all(15),
                child: Column(children: [
                  // this._getTituloArticulo(e['Capitulo']),
                  this._getArticuloTitulo(e['Titulo']),
                  this._getContenidoItems(e['Contenido'], e['ITEMS'], context),
                  // this._getArticuloContenido(e['Contenido']),
                  // this._getITEMS(e['ITEMS'], context),
                ]),
              );
            }).toList(),
          );
  }

  // Widget _getTituloArticulo(String titulo){
  //   return Container(
  //     // height: 2,
  //     child: Text(titulo),
  //   );
  // }
  Widget _getArticuloTitulo(String titulo) {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      alignment: Alignment.centerLeft,
      child: Text(
        titulo,
        style: TextStyle(
            color: Constant.colorLogoBack,
            fontWeight: FontWeight.bold,
            fontSize: 15),
      ),
    );
  }

  Widget _getArticuloContenido(String contenido) {
    return Container(
      child: SelectableText.rich(
        TextSpan(
          children: [
            TextSpan(text: contenido),
          ],
        ),
      ),
    );
  }

  // Widget _getITEMS(List itemsDentro, BuildContext context) {
  //   return itemsDentro == null
  //       ? Container()
  //       : Container(
  //           child: Row(
  //             // mainAxisAlignment: MainAxisAlignment.center,
  //             children: itemsDentro.map((e) {
  //               return Container(
  //                 margin: EdgeInsets.only(right: 10),
  //                 child: InkWell(
  //                   child: Text(
  //                     e['titulo'],
  //                     style: TextStyle(
  //                       color: Color(0xFFA44702),
  //                       fontSize: 15,
  //                     ),
  //                   ),
  //                   onTap: () {
  //                     // print('presionado :' + e['titulo']);
  //                     showDialog(
  //                       context: context,
  //                       builder: (BuildContext context) {
  //                         return AlertDialog(
  //                           title: Row(
  //                             mainAxisAlignment: MainAxisAlignment.start,
  //                             children: [
  //                               IconButton(
  //                                 icon: Icon(
  //                                   Icons.arrow_back,
  //                                   color: Colors.blueGrey,
  //                                 ),
  //                                 onPressed: () {
  //                                   Navigator.of(context).pop();
  //                                 },
  //                               ),
  //                               Text(
  //                                 e['titulo'],
  //                                 style: TextStyle(
  //                                   // color: Color(0xFFA44702),
  //                                   color: Color.fromRGBO(135, 24, 48, 1),
  //                                 ),
  //                               ),
  //                             ],
  //                           ),
  //                           content: SingleChildScrollView(
  //                             child: ListBody(
  //                               children: <Widget>[
  //                                 Center(
  //                                   child: Text(e['contenido']),
  //                                 ),
  //                               ],
  //                             ),
  //                           ),
  //                         );
  //                       },
  //                     );
  //                   },
  //                 ),
  //               );
  //             }).toList(),
  //           ),
  //         );
  // }

  Widget _getContenidoItems(
      String contenido, List itemsDentro, BuildContext context) {
    int cantidad = itemsDentro.length;
    String contTemp = contenido;
    // contTemp = contTemp.replaceAll("\r\n\r\n", "\n");
    // print("cantidad -> : " + cantidad.toString());

    if (cantidad == 0) {
      // print("================================================================");
      return this._getArticuloContenido(contenido);
    } else {
      List<Widget> widgets = [];

      for (var i = 0; i < cantidad; i++) {
        // print('proceso: '+(i+1).toString());
        // print("contenido todal: "+contTemp);
        // print('se busca: '+itemsDentro[i]['titulo'].toString() + " en: "+contTemp);
        // print('se busca: '+itemsDentro[i]['titulo'].toString() + " en: contenido");
        List split = contTemp.split(itemsDentro[i]['titulo']);
        // print(split);

        widgets.add(Text(split[0]));
        //modal
        // widgets.add(Text (itemsDentro[i]['titulo'],style: TextStyle(color:Colors.red),));
        widgets.add(_insertModal(
            itemsDentro[i]['titulo'], itemsDentro[i]['contenido'], context));
        //endmodal
        String temp = '';
        for (var j = 0; j < split.length; j++) {
          // 0  , 1  , 2   (3)
          if (j > 0) {
            if (j == split.length - 1) {
              temp = temp + split[j];
            } else {
              temp = temp + split[j] + itemsDentro[i]['titulo'];
            }
          }
        }
        if ((i + 1 == cantidad) && split.length == 2) {
          // print("IF FINAL: ");
          widgets.add(Text(split[1]));
        }
        contTemp = temp;
      }
      // print("================================================================");
      return Container(
        // padding: EdgeInsets.only(left: 10, right: 10),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: widgets),
      );
    }
  }

  Widget _insertModal(String titulo, String cont, BuildContext context) {
    return InkWell(
      child: Text(
        titulo,
        style: TextStyle(
          color: Color(0xFFA44702),
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {
        // print('presionado :' + cont);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Row(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Constant.colorLogoBack,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  Center(
                      child: Text(titulo,
                          style: TextStyle(
                            color: Color.fromRGBO(135, 24, 48, 1),
                          ))),
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Center(
                      child: Text(cont),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: Text('Ok',
                      style: TextStyle(color: Constant.colorLogoBack)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }
}
