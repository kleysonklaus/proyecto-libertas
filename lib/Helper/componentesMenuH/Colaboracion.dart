import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';

class Colaboracion extends StatelessWidget {
  final double apu; //anchoPopUp
  final double alpu; //altoPopUp
  final double mt; //margenTodo
  final double sep; //separacion

  const Colaboracion({Key key, this.apu, this.alpu, this.mt, this.sep})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(Icons.credit_card, color: Constant.colorIconsPopUp),
          SizedBox(width: sep),
          Text('Colaboración',
              style: TextStyle(color: Constant.colorIconsPopUp))
        ]));
  }
}
