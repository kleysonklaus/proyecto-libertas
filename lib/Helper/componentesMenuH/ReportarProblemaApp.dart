import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/componentesMenuH/_ReportProblem.dart';

class ReportarProblemaApp extends StatelessWidget {
  final double apu; //anchoPopUp
  final double alpu; //altoPopUp
  final double mt; //margenTodo
  final double sep; //separacion

  const ReportarProblemaApp({Key key, this.apu, this.alpu, this.mt, this.sep})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(mt),
      width: apu,
      height: alpu,
      child: InkWell(
        child: Row(
          children: [
            Icon(Icons.notifications_active_outlined,
                color: Constant.colorIconsPopUp),
            SizedBox(width: sep),
            Text('Reportar Problema',
                style: TextStyle(color: Constant.colorIconsPopUp))
          ],
        ),
        onTap: () {
          //IMPORTANTE => CIERRA EL BACK LIST EN 1 MODAL
          Navigator.pop(context);
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return alertDialogReportProblem(context);
            },
          );
        },
      ),
    );
  }

  Widget alertDialogReportProblem(BuildContext context) {
    return AlertDialog(
        elevation: 30,
        title: Text('REPORTAR UN PROBLEMA',
            style: TextStyle(color: Constant.primaryColor)),
        content: Padding(
          padding: const EdgeInsets.all(1),
          child: Container(
              height: 380,
              width: 350,
              //CLASES DE LOS CHIPS DE PROBLEMA ReportProblem.dart
              child: ReportProblemApp()),
        )
        // actions: <Widget>[
        //   TextButton(
        //     child: Text(
        //       'Cancelar',
        //       style: TextStyle(color: Constant.colorLogoBack),
        //     ),
        //     onPressed: () {
        //       Navigator.pop(context);
        //       print('Boton de cancelar presionado');
        //     },
        //   ),
        //   TextButton(
        //     child: Text(
        //       'Enviar',
        //       style: TextStyle(color: Constant.colorLogoBack),
        //     ),
        //     onPressed: () {
        //       Navigator.pop(context);
        //       print('Boton de enviar presionado');
        //     },
        //   ),
        // ],
        );
  }

  // Widget getReportarProblema(
  //     double apu, double alpu, double mt, double sep, BuildContext context) {
  //   return Container(
  //     margin: EdgeInsets.all(mt),
  //     width: apu,
  //     height: alpu,
  //     child: InkWell(
  //       child: Row(
  //         children: [
  //           Icon(
  //             Icons.notifications_active_outlined,
  //             color: Constant.colorIconsPopUp,
  //           ),
  //           SizedBox(width: sep),
  //           Text(
  //             'Reportar Problema',
  //             style: TextStyle(color: Constant.colorIconsPopUp),
  //           )
  //         ],
  //       ),
  //       onTap: () {
  //         //IMPORTANTE => CIERRA EL BACK LIST EN 1 MODAL
  //         Navigator.pop(context);
  //         showDialog(
  //           context: context,
  //           builder: (BuildContext context) {
  //             return alertDialogReportProblem(context);
  //           },
  //         );
  //       },
  //     ),
  //   );
  // }

}
