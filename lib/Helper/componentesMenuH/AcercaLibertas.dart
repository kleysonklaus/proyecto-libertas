import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';

class AcercaLibertas extends StatelessWidget {
  final double apu; //anchoPopUp
  final double alpu; //altoPopUp
  final double mt; //margenTodo
  final double sep; //separacion

  const AcercaLibertas({Key key, this.apu, this.alpu, this.mt, this.sep})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(mt),
        width: apu,
        height: alpu,
        child: Row(children: [
          Icon(Icons.bookmark_border, color: Constant.colorIconsPopUp),
          SizedBox(width: sep),
          Text('Acerca de LIBERTAS',
              style: TextStyle(color: Constant.colorIconsPopUp))
        ]));
  }
}
