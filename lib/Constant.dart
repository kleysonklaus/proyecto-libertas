import 'package:flutter/material.dart';
import 'package:libertas6/Classes/MyChip.dart';
// AUMENTADO
import 'package:libertas6/Classes/ChipsReportProblems.dart';

abstract class Constant {
  static String apiUrl = 'https://libertas.pe/legislacion';
  static String apiUrlJuris = 'https://libertas.pe/jurisprudencia';
  static String apiPeruanoFechas = '/get_elperuano_fechas/';

  static double containerMarginLeft = 20;
  static double containerMarginTop = 20;
  static double containerMarginRight = 20;
  static double containerMarginBottom = 20;

  static Color primaryColor = Color(0xFF040466);
  static Color disabledColor = Color(0xFFB7B7B7);
  static Color contentColor = Color(0xFF060605);
  static Color colorLogoBack = Color.fromRGBO(4, 20, 79, 1);
  static Color colorIconsPopUp = Color.fromRGBO(96, 96, 96, 1);
  // static Color colorIconsPopUp = Colors.grey;

  static List<MyChip> _getLegislacionChips() {
    List<MyChip> list = new List();
    const String PATH = '/get_busquedacontenido/';

    list.add(
      new MyChip('Todos los contenidos', '', false, null),
    );
    list.add(new MyChip('Código Penal', PATH, true, 36));
    list.add(new MyChip('Código Procesal Penal', PATH, false, 38));
    list.add(new MyChip('Nuevo Código Procesal Penal', PATH, false, 39));
    list.add(new MyChip('Código de Procedimientos Penales', PATH, false, 41));
    list.add(new MyChip('Código civil', PATH, false, 35));
    list.add(new MyChip('Código procesal civil', PATH, false, 42));
    list.add(new MyChip('Código Procesal Constitucional', PATH, false, 43));
    list.add(new MyChip('Código de los Niños y Adolescentes', PATH, false, 45));
    list.add(
        new MyChip('Código de P. y Defensa del Consumidor', PATH, false, 46));

    // list.add(
    //   new MyChip('Todos los contenidos', '', true),
    // );
    // list.add(new MyChip('Constitución Politica del Perú',
    //     '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip('Código Procesal Constitucional',
    //     '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip(
    //     'Código  Civil', '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip('TUO Código Precesal Civil',
    //     '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip(
    //     'Código Penal', '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip('Nuevo Código Procesal Penal',
    //     '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip('Código Procesal Penal',
    //     '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip('Código de Procedimientos Penales',
    //     '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip('Código de los Niños y Adolescente',
    //     '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip('Código de Prot. y Defensa del Consumidor',
    //     '/get_busquedacontenido/documento/36,36/', false));
    // list.add(new MyChip('Ley Procesal del Trabajo',
    //     '/get_busquedacontenido/documento/36,36/', false));
    return list;
  }

  static List<MyChip> legislacionChips = _getLegislacionChips();

//CHIPS DE PROBLEMAS

  static List<ChipProblem> _getProblemsChips() {
    List<ChipProblem> list = new List();

    list.add(new ChipProblem('Todo funciona bien. Gracias', false));
    list.add(new ChipProblem('Falta actualizar algunos artículos', false));
    list.add(new ChipProblem('No encuentro algunos artículos', false));
    list.add(new ChipProblem('No carga una parte de la norma', false));
    list.add(new ChipProblem('Otro', false));
    return list;
  }

  static List<ChipProblem> problemChips = _getProblemsChips();
}
