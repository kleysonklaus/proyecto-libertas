import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:libertas6/Pages/Home.dart';
// import 'package:libertas6/login.dart';
import 'Constant.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(
      Duration(milliseconds: 1500),
      () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          // builder: (context) => new Login(),
          builder: (context) => new Home(),
        ),
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constant.colorLogoBack,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FractionallySizedBox(
              // widthFactor: 0.6,
              child: _getLogo(),
            ),
            // SizedBox(height: 200),
            SizedBox(height: 25),
            CircularProgressIndicator(),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }

  Widget _getLogo() {
    return Container(
      // height: 450,
      color: Constant.colorLogoBack,
      child: Stack(
        children: <Widget>[
          Positioned(
            child: Container(
              // color: Colors.black,
              margin: EdgeInsets.only(top: 50),
              child: Center(
                child: Image(
                  image: AssetImage('assets/images/logo.jpg'),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
