class MyChip {
  String _label;
  String _url;
  bool _selected;
  int _numNorma;

  MyChip(String label, String url, bool selected, int numNorma) {
    this._label = label;
    this._url = url;
    this._selected = selected;
    this._numNorma = numNorma;
  }

  String getLabel() {
    return this._label;
  }

  int getNumNorma() {
    return this._numNorma;
  }

  void setLabel(String _label) {
    this._label = _label;
  }

  String getUrl() {
    return this._url;
  }

  void setUrl(String _url) {
    this._url = _url;
  }

  bool isSelected() {
    return this._selected;
  }

  void setSelected(bool _selected) {
    this._selected = _selected;
  }
}
