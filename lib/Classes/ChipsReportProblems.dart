class ChipProblem {
  String _label;
  bool _selected;

  ChipProblem(String label, bool selected) {
    this._label = label;
    this._selected = selected;
  }

  String getLabel() {
    return this._label;
  }

  void setLabel(String _label) {
    this._label = _label;
  }

  bool isSelected() {
    return this._selected;
  }

  void setSelected(bool _selected) {
    this._selected = _selected;
  }
}
