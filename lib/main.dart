import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:libertas6/Constant.dart';
// import 'package:libertas6/login.dart';
import 'package:libertas6/splashScreen.dart';
// import 'Pages/Home.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final FocusScopeNode focus = FocusScope.of(context);
        if (!focus.hasPrimaryFocus && focus.hasFocus) {
          FocusManager.instance.primaryFocus.unfocus();
        }
      },
      child: MaterialApp(
        localizationsDelegates: [GlobalMaterialLocalizations.delegate],
        supportedLocales: [const Locale('es')],
        debugShowCheckedModeBanner: false,
        title: 'Title',
        theme: ThemeData(primaryColor: Constant.colorLogoBack),
        // home: new Home(),
        // home: new Login(),
        home: new SplashScreen(),
      ),
    );
  }
}
