import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';

// import '../Constant.dart';

class ElPeruanoDetalle extends StatefulWidget {
  final String id;

  const ElPeruanoDetalle({Key key, this.id}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _ElPeruanoDetalleState();
  }
}

class _ElPeruanoDetalleState extends State<ElPeruanoDetalle> {
  Map<String, dynamic> _post;

  @override
  void initState() {
    super.initState();
    this._setPosts();
  }

  // obtiene un solo contenido por fecha de la API
  void _setPosts() async {
    RequestHelper.getObjectFromAPI(
            '/get_elperuanoleycontenido/' + this.widget.id)
        .then((response) {
      setState(() {
        this._post = response;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [this._getContent()];
    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  // arma el loading y resultados de contenido de la API
  Widget _getContent() {
    return this._post == null
        ? WidgetHelper.getWidgetLoader()
        : Container(
            padding: EdgeInsets.all(20),
            height: MediaQuery.of(context).size.height - 95,
            // margin: EdgeInsets.only(
            //   top: Constant.containerMarginTop,
            //   // bottom: Constant.containerMarginBottom,
            //   bottom: MediaQuery.of(context).size.height-250,
            //   left: Constant.containerMarginLeft,
            //   right: Constant.containerMarginRight,
            // ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  this._getItemTitle(this._post['titulo']),
                  this._getItemSubTitle(this._post['subtitulo']),
                  this._getItemContent(this._post['contenido'])
                ],
              ),
            ),
          );
  }

  // arma el titulo del item en respuesta
  Widget _getItemTitle(title) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      width: double.infinity,
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 17,
          color: Constant.primaryColor,
        ),
      ),
    );
  }

  // arma el subtitulo del item en respuesta
  Widget _getItemSubTitle(subtitle) {
    return Container(
      margin: EdgeInsets.only(bottom: 12),
      width: double.infinity,
      child: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 15,
        ),
      ),
    );
  }

  // arma el contenido del item en respuesta
  Widget _getItemContent(content) {
    return Container(
      child: Text(
        content,
        textAlign: TextAlign.justify,
        style: TextStyle(
          fontSize: 14,
        ),
      ),
    );
  }
}
