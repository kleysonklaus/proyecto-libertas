import 'package:flutter/material.dart';
import 'package:simple_pdf_viewer/simple_pdf_viewer.dart';

class VisorPDF extends StatefulWidget {
  final String sourceLink;

  const VisorPDF({Key key, this.sourceLink}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _VisorPDFState();
  }
}

class _VisorPDFState extends State<VisorPDF> {
  @override
  void initState() {
    super.initState();
  }

  // NO SE USA ESTE ARCHIVO, POR DEFICIENCIA Y RALENTIZACION DE PDF
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          color: Colors.grey,
          margin: EdgeInsets.all(0),
          child: new SimplePdfViewerWidget(
            initialUrl: this.widget.sourceLink,
          )),
    );
  }
}
