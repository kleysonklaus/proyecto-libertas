import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/Helper.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
import 'package:libertas6/Pages/ElPeruano/ElPeruanoDetalle.dart';
// import 'package:libertas6/Pages/VisorPDF.dart';
import 'package:libertas6/Pages/ElPeruano/VisorPDF2.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class ElPeruano extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _ElPeruanoState();
  }
}

class _ElPeruanoState extends State<ElPeruano> {
  Map<String, dynamic> _posts;
  List<DateTime> _postsFechas = List();
  DateTime _date;
  String _localDate = '';
  int localDay, localYear, numMonth;
  String localMonth = '';
  bool existe = false;

  @override
  void initState() {
    super.initState();
    this._date = DateTime.now();
    this._setPosts();
    this._setPostsFechas();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getAppMenuTopProfile(),
      // WidgetHelper.getBuscador('legislacion', context),
      this._getTitle(),
      this._getPosts()
    ];
    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  // obtiene datos de la API e actualiza estado de widget con la fecha (ultima)
  void _setPosts() async {
    String date = this._date.year.toString() +
        '-' +
        this._date.month.toString() +
        '-' +
        this._date.day.toString();
    print('-----------------------');
    print(date);
    RequestHelper.getObjectFromAPI('/get_elperuanocontenido/' + date).then(
      (response) {
        setState(
          () {
            this._posts = response;
            this._localDate = _posts['fecha'];
            print('fecha ultimo actualizado: ' + _localDate.toString());
            localYear = Helper.numYear(_localDate);
            localMonth = Helper.nameMonth(_localDate);
            numMonth = Helper.numMonth(_localDate);
            localDay = Helper.numDay(_localDate);
            // _date = new DateTime.utc(localYear, localDay, numMonth);
            _date = new DateTime.utc(localYear, numMonth, localDay);
            // existe = true;
          },
        );
      },
    );
  }

  // obtiene todas las fechas que tienen contenido
  void _setPostsFechas() async {
    RequestHelper.getListFromAPI(Constant.apiPeruanoFechas).then(
      (response) {
        setState(
          () {
            // this._postsFechas = response;
            // print('----fechas--viejas---');
            // for (var fech in response) {
            //   print('=== ' + fech['fecha']);
            // }
            // print('convirtiendo!!!');
            for (var item in response) {
              int anio = Helper.numYear(item['fecha']);
              int mes = Helper.numMonth(item['fecha']);
              int dia = Helper.numDay(item['fecha']);
              _postsFechas.add(new DateTime(anio, mes, dia));
            }
            // print('----fechas--nuevas---');
            // print(_postsFechas);
            // print(_postsFechas[0].month);
            existe = true;
          },
        );
      },
    );
  }

  void _resetData() {
    this._posts = null;
    this._setPosts();
  }

  // arma el titulo de el peruano
  Widget _getTitle() {
    return Container(
      width: double.infinity,
      child: IntrinsicHeight(
        child: Row(
          children: [
            Expanded(
              child: this._getLogoElPeruano(),
            ),
            this._getIconoCalendario(),
            this._getIconoPDF()
          ],
        ),
      ),
    );
  }

  Widget _getLogoElPeruano() {
    return Container(
      child: Column(
        children: [
          Container(
            // color: Colors.black,
            child: Image(
              image: AssetImage('assets/images/logo_elperuano.jpg'),
              height: 60,
              width: 300,
              alignment: Alignment.topLeft,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(left: 20),
            child: this._posts == null
                ? Container()
                : Text(
                    Helper.getDiaSemanaNombre(this._date.weekday - 1) +
                        ', ' +
                        _date.day.toString() +
                        ' de ' +
                        Helper.getMesNombre(_date.month - 1).toString() +
                        ' ' +
                        _date.year.toString(),
                    style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold),
                  ),
          )
        ],
      ),
    );
  }

  // arma el icono del calendario y su estado para actualizar el contenido
  Widget _getIconoCalendario() {
    return Container(
      width: 48,
      alignment: Alignment.center,
      child: existe
          ? InkWell(
              child: Image(
                image: AssetImage('assets/icons/png/calendario.png'),
                width: 32,
                color: Constant.colorLogoBack,
              ),
              onTap: () => this._selectDate(context),
            )
          : Image(
              image: AssetImage('assets/icons/png/calendario.png'),
              width: 32,
              color: Color.fromRGBO(127, 127, 127, 1),
            ),
    );
  }

  // recorrido de todas las fechas que contienen datos, para bloquear los que no tienen
  bool encuentraFecha(DateTime val) {
    // String _fecha = temp.year.toString() +
    //     '-' +
    //     temp.month.toString() +
    //     '-' +
    //     temp.day.toString();
    // print('fecha a buscar ' + _fecha);
    for (var fecha in _postsFechas) {
      if (fecha.year == val.year &&
          fecha.month == val.month &&
          fecha.day == val.day) {
        return false;
      }
    }
    return true;
  }

  // arma el widget para la seleccion de la fecha
  Future _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      selectableDayPredicate: (DateTime val) =>
          this.encuentraFecha(val) ? false : true,
      // {
      //   if (this.encuentraFecha(val))
      //     return false;
      //   else
      //     return true;
      //   // val.weekday == 5 || val.weekday == 6 ? false : true,
      // },
      context: context,
      initialDate: this._date,
      firstDate: DateTime(2020),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light(
              primary: Constant.colorLogoBack,
              onPrimary: Colors.white,
              surface: Constant.colorLogoBack,
              onSurface: Colors.black,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child,
        );
      },
    );

    if (picked != null && picked != this._date) {
      setState(() {
        this._date = picked;
        this._resetData();
      });
    }
  }

  // arma el icono del pdf y la redireccion para ver el pdf
  Widget _getIconoPDF() {
    return Container(
        width: 48,
        alignment: Alignment.center,
        child: this._posts == null
            ? ImageIcon(
                AssetImage('assets/icons/png/pdf.png'),
                color: Colors.grey,
                size: 32,
              )
            : InkWell(
                child: Image(
                    image: AssetImage('assets/icons/png/pdf.png'), width: 32),
                onTap: () {
                  pushNewScreen(
                    context,
                    // screen: VisorPDF(sourceLink: this._posts['pdf']),
                    screen: ShowPdf(pdfLink: this._posts['pdf']),

                    withNavBar: true, // OPTIONAL VALUE. True by default.
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  );
                },
              ));
  }

  // arma la pantalla de carga y resultados de respuesta de la API
  Widget _getPosts() {
    return this._posts == null
        ? WidgetHelper.getWidgetLoader()
        : Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: this._posts['data'].map<Widget>(
                  (e) {
                    return Container(
                      margin: EdgeInsets.only(
                          left: Constant.containerMarginLeft,
                          right: Constant.containerMarginRight,
                          top: 10,
                          bottom: Constant.containerMarginBottom),
                      child: Column(
                        children: [
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(bottom: 10),
                            child: Text(
                              e['Titulo'],
                              style: TextStyle(
                                  // color: Constant.primaryColor,
                                  color: Color.fromRGBO(0, 0, 78, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                          ),
                          Container(
                            child: Column(
                              children: e['Leyes'].map<Widget>((f) {
                                return Column(children: [
                                  this._getItemTitle(
                                      f['Titulo2'], f['id_ley'], f['pdf']),
                                  this._getItemContent(f['Contenido'])
                                ]);
                              }).toList(),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ).toList(),
              ),
            ),
          );
  }

  // arma el titulo del cada item
  Widget _getItemTitle(String title, String id, String pdf) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 3),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: title,
              style: TextStyle(
                  color: Color.fromRGBO(52, 43, 147, 1) /*Colors.blue*/,
                  fontWeight: FontWeight.bold),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  pushNewScreen(
                    context,
                    screen: ElPeruanoDetalle(id: id),
                    withNavBar: true, // OPTIONAL VALUE. True by default.
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  );
                },
            ),
            WidgetSpan(
              alignment: PlaceholderAlignment.bottom,
              style: TextStyle(),
              child: Container(
                margin: EdgeInsets.only(left: 5),
                child: InkWell(
                  child: ImageIcon(
                    AssetImage('assets/icons/png/pdf.png'),
                    color: Colors.red,
                    size: 14,
                  ),
                  onTap: () {
                    print(pdf);
                    pushNewScreen(
                      context,
                      // screen: VisorPDF(sourceLink: pdf),
                      screen: ShowPdf(pdfLink: pdf), //NUEVO VISOT DE PDF
                      withNavBar: true, // OPTIONAL VALUE. True by default.
                      pageTransitionAnimation:
                          PageTransitionAnimation.cupertino,
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _getItemContent(content) {
    return Container(
      child: Text(content, style: TextStyle(fontSize: 13)),
    );
  }
}
