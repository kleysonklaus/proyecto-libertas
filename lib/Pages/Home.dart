import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Pages/ElPeruano/ElPeruano.dart';
import 'package:libertas6/Pages/Inicio/Inicio.dart';
import 'package:libertas6/Pages/Jurisprudencia/Jurisprudencia.dart';
import 'package:libertas6/Pages/Legislacion/Legislacion.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  final double _iconSize = 1.7;
  PersistentTabController _controller =
      PersistentTabController(initialIndex: 0);

  bool hideNavBar;

  @override
  void initState() {
    super.initState();
    hideNavBar = false;
  }

  // construye el menu principal
  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      controller: _controller,
      hideNavigationBar: hideNavBar, // OCULTAR BARRA DE NAVEGACION
      screens: _buildScreens(),
      items: _navBarsItems(),
      navBarHeight: 80,
      // padding: NavBarPadding.only(top: 23, bottom: 23),
      padding: NavBarPadding.only(top: 16, bottom: 15),
      confineInSafeArea: true,
      backgroundColor: Colors.white, // COLOR DE FONDO MENU
      handleAndroidBackButtonPress: true,
      resizeToAvoidBottomInset:
          true, // This needs to be true if you want to move up the screen when keyboard appears.
      stateManagement: true,
      hideNavigationBarWhenKeyboardShows:
          true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: Colors.white,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: ItemAnimationProperties(
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation(
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle:
          NavBarStyle.style6, // Choose the nav bar style with this property.
    );
  }

  // llamada a las pantallas principales
  List<Widget> _buildScreens() {
    return [
      new Inicio(),
      new Legislacion(),
      new Jurisprudencia(),
      new ElPeruano()
    ];
  }

  // construccion de los iconos para las pantallas principales
  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Container(
          child: Transform.scale(
            scale: this._iconSize,
            child: ImageIcon(
              AssetImage('assets/icons/png/inicio.png'),
            ),
          ),
        ),
        title: ("Inicio"),
        activeColor: Constant.primaryColor,
        inactiveColor: CupertinoColors.systemGrey,
        titleFontSize: 13,
      ),
      PersistentBottomNavBarItem(
        icon: Container(
          child: Transform.scale(
            scale: this._iconSize,
            child: ImageIcon(
              AssetImage('assets/icons/png/legislacion.png'),
            ),
          ),
        ),
        title: ("Legislación"),
        activeColor: Constant.primaryColor,
        inactiveColor: CupertinoColors.systemGrey,
        titleFontSize: 13,
      ),
      PersistentBottomNavBarItem(
        icon: Container(
          child: Transform.scale(
            scale: this._iconSize,
            child: ImageIcon(
              AssetImage('assets/icons/png/jurisprudencia.png'),
            ),
          ),
        ),
        title: ("Jurisprudencia"),
        activeColor: Constant.primaryColor,
        inactiveColor: CupertinoColors.systemGrey,
        titleFontSize: 13,
      ),
      PersistentBottomNavBarItem(
        icon: Container(
          child: Transform.scale(
            scale: this._iconSize,
            child: ImageIcon(
              AssetImage('assets/icons/png/el_peruano.png'),
            ),
          ),
        ),
        title: ("El Peruano"),
        activeColor: Constant.primaryColor,
        inactiveColor: CupertinoColors.systemGrey,
        titleFontSize: 13,
      )
    ];
  }
}
