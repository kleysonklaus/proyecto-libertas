import 'package:flutter/material.dart';
import 'package:libertas6/Classes/MyChip.dart';
import 'package:libertas6/Constant.dart';
// import 'package:libertas6/Fonts/libertas_icons.dart';
// import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';

// ############
// import 'package:libertas6/Modals/ChipsModal.dart';
import 'package:libertas6/Pages/Inicio/LegislacionResultados.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
// import '../../Modals/ChipsModal.dart';

class Inicio extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _InicioState();
  }
}

class _InicioState extends State<Inicio> {
  List<MyChip> myChips = List();
  int minimun = 1;
  double _distanceTopBot = 40;
  TextEditingController textoBuscar = TextEditingController();
  bool enfocado = false;
  FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();
    enfocado = false;
    myFocusNode = FocusNode();

    myChips = Constant.legislacionChips;
    print(myChips.length);
    print(this.countSelected(myChips));
    if (this.myChips[0].isSelected()) {
      this.selectAllChips(myChips);
    }
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      // WidgetHelper.getLogoTopAnimated(context),
      // this._appBarTopLogo(),
      WidgetHelper.getAppMenuTopProfile(),
      this._getLocalBuscador("legislacion", context),
      this._getLocalChips(),
      // ChipsModal("legislacion"),
    ];

    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  // arma el buscador generico,
  Widget _getLocalBuscador(String typeOfChip, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          top: _distanceTopBot / 2, bottom: _distanceTopBot / 2),
      child: Center(
        child: Container(
          color: Color(0xFFF4F3FB),
          margin: EdgeInsets.only(
              left: Constant.containerMarginLeft,
              right: Constant.containerMarginRight),
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  onSubmitted: (escrito) {
                    print(escrito);
                    pushNewScreen(
                      context,
                      screen: LegislacionResultados(
                          palabraBuscar: textoBuscar.text.toUpperCase()),
                      withNavBar: true, // OPTIONAL VALUE. True by default.
                      pageTransitionAnimation:
                          PageTransitionAnimation.cupertino,
                    );
                    FocusScope.of(context).unfocus();
                    textoBuscar.clear();
                  },
                  controller: textoBuscar,
                  autofocus: enfocado,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: '¿ Qué desea buscar ?',
                    suffixIcon: IconButton(
                      onPressed: () => textoBuscar.clear(),
                      icon: Icon(
                        Icons.clear,
                        color: Constant.colorLogoBack,
                      ),
                    ),
                  ),
                ),
              ),
              // Container(
              //   margin: EdgeInsets.only(right: 10),
              //   child: IconButton(
              //     icon: Icon(Icons.search),
              //     onPressed: () {
              //       pushNewScreen(
              //         context,
              //         screen: LegislacionResultados(
              //             palabraBuscar: textoBuscar.text.toUpperCase()),
              //         withNavBar: true, // OPTIONAL VALUE. True by default.
              //         pageTransitionAnimation:
              //             PageTransitionAnimation.cupertino,
              //       );
              //       FocusScope.of(context).unfocus();
              //       textoBuscar.clear();
              //     },
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  // obtiene e instancia objetos de la clase de chips
  Widget _getLocalChips() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: ListView(
          children: [
            Wrap(
              spacing: 10.0,
              runSpacing: 1.0,
              children: myChips
                  .map(
                    (chip) => FilterChip(
                      label: Text(
                        chip.getLabel(),
                        style: TextStyle(
                            color: chip.isSelected()
                                ? Colors.white
                                : Colors.black),
                      ),
                      selected: chip.isSelected(),
                      selectedColor: Constant.primaryColor,
                      showCheckmark: false,
                      onSelected: (bool selected) {
                        setState(
                          () {
                            chip.setSelected(selected);
                          },
                        );
                        this.verifyAllChips(myChips, myChips.indexOf(chip));
                      },
                    ),
                  )
                  .toList(),
            ),
          ],
        ),
      ),
    );
    // return Container(
    //   color: Colors.brown,
    //   child: Wrap(
    //     spacing: 10.0,
    //     runSpacing: 1.0,
    //     children: myChips
    //         .map(
    //           (chip) => FilterChip(
    //             label: Text(
    //               chip.getLabel(),
    //               style: TextStyle(
    //                   color: chip.isSelected() ? Colors.white : Colors.black),
    //             ),
    //             selected: chip.isSelected(),
    //             selectedColor: Constant.primaryColor,
    //             showCheckmark: false,
    //             onSelected: (bool selected) {
    //               setState(
    //                 () {
    //                   chip.setSelected(selected);
    //                 },
    //               );
    //               this.verifyAllChips(myChips, myChips.indexOf(chip));
    //             },
    //           ),
    //         )
    //         .toList(),
    //   ),
    // );
  }

  // $##################################### funcionalidad

  // cada vez que se hace cambios en la pantalla, se llama a esta funcion para cambiar estados y ver en pantalla
  void verifyAllChips(List myChips, int indx) {
    print("verificando ... indice = " + indx.toString());
    if (indx == 0) {
      if (this.myChips[0].isSelected()) {
        this.selectAllChips(myChips);
      } else {
        this.noSelectAllChips(myChips);
      }
    } else {
      this.myChips[0].setSelected(false);
    }

    if (countSelected(myChips) == myChips.length - 1) {
      this.myChips[0].setSelected(true);
    }

    print("seleccionados: " + this.countSelected(myChips).toString());
    print("no seleccionados: " + this.countNoSelected(myChips).toString());
  }

  // todos los chips seleccionados
  void selectAllChips(List myChips) {
    for (var i in this.myChips) {
      i.setSelected(true);
    }
  }

  // todos los chips en no seleccionados
  void noSelectAllChips(List myChips) {
    for (var i in this.myChips) {
      i.setSelected(false);
    }
  }

  // contar cuantos estan seleccionados
  int countSelected(List myChips) {
    int count = 0;
    for (var i in myChips) {
      if (i.isSelected()) {
        count += 1;
      }
    }
    return count;
  }

  // contar cuantos no estan seleccionados
  int countNoSelected(List myChips) {
    int count = 0;
    for (var i in myChips) {
      if (!i.isSelected()) {
        count += 1;
      }
    }
    return count;
  }
}
