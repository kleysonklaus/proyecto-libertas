import 'dart:collection';

import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:libertas6/Classes/MyChip.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
import 'package:loading_indicator/loading_indicator.dart';

// import '../Constant.dart';

class LegislacionResultados extends StatefulWidget {
  final String clave;
  final String palabraBuscar;

  const LegislacionResultados({Key key, this.clave, this.palabraBuscar})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LegislacionResultadosState();
  }
}

class _LegislacionResultadosState extends State<LegislacionResultados>
    with TickerProviderStateMixin {
  List<MyChip> _chips;
  HashMap<String, List<dynamic>> _posts;
  TabController _tabController;
  String _palabra = '';
  bool resultados = false;

  @override
  void initState() {
    super.initState();
    this._palabra = this.widget.palabraBuscar;
    print('palabra de text: ' + widget.palabraBuscar);

    this._posts = new HashMap();
    this._posts['Todos los contenidos'] = new List<dynamic>();

    // this._chips = Constant.legislacionChips
    //     .where(
    //       (element) => element.isSelected(),
    //     )
    //     .toList();
    //--------------------------------------
    // this._chips = Constant.legislacionChips
    //     .where(
    //       (element) => element.getLabel() == 'Todos los contenidos',
    //     )
    //     .toList();
    this._chips = Constant.legislacionChips
        // .where(
        //   (element) => element.isSelected(),
        // )
        .where((element) => element.getLabel() == 'Todos los contenidos')
        .toList();
    _tabController = new TabController(length: this._chips.length, vsync: this);
    this._setPosts();
  }

  // arma el tab contenido verificando si hay contenido para armar el tab
  void _setPosts() async {
    Constant.legislacionChips.forEach(
      (element) {
        print("elemeto es: -> " + element.toString());
        if (element.isSelected() && element.getUrl() != '') {
          RequestHelper.getListFromAPI(this._getMakeUrl(
                  element.getUrl(), element.getNumNorma(), _palabra))
              .then(
            (response) {
              if (response.isNotEmpty) {
                setState(() {
                  this._posts[element.getLabel()] = response;
                  response.forEach(
                    (e) {
                      e['etiqueta'] = element.getLabel();
                      print('cada elemtoooooo buscadooo en: ' +
                          e['etiqueta'].toString());
                    },
                  );
                  this._posts['Todos los contenidos'].addAll(response);

                  this._chips.add(
                        new MyChip(element.getLabel(), '', true, null),
                      );
                  _tabController = new TabController(
                      length: this._chips.length, vsync: this);
                  resultados = true;
                });
              } else {
                print("NO SE LLENA");
              }
            },
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getAppMenuTopProfile(),
      this._getTabs(),
      this._getTabContent(),
    ];
    // return WidgetHelper.getStaticContainer(widgets);

    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  String _getMakeUrl(String url, int numNorma, String palabra) {
    print(url + palabra + '/' + numNorma.toString() + '/');
    url = url + palabra + '/' + numNorma.toString() + '/';
    return url;
  }

  // arma los tab con el contenido de respuesta de la API
  Widget _getTabs() {
    return Container(
      child: TabBar(
        controller: this._tabController,
        indicatorSize: TabBarIndicatorSize.label,
        labelColor: Constant.primaryColor,
        unselectedLabelColor: Constant.disabledColor,
        isScrollable: true,
        onTap: (value) {
          // print(value);
        },
        tabs: this
            ._chips
            .map(
              (e) => Tab(
                text: e.getLabel(),
              ),
            )
            .toList(),
      ),
    );
  }

  // arma el contenido para cada tabulador de contenido
  Widget _getTabContent() {
    return Container(
      child: Expanded(
        child: TabBarView(
          controller: _tabController,
          children: this
              ._chips
              .map(
                (e) => this._getTabResults(e.getLabel()),
              )
              .toList(),
        ),
      ),
    );
  }

  // muestra la pantalla de carga y resultados de la API
  Widget _getTabResults(String label) {
    // if (!this._posts.containsKey(label)) {
    //   return WidgetHelper.getWidgetLoader();
    if (!resultados) {
      return ListView(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(top: 5, left: 25),
            child: Text(
              'Buscando en la Base de Datos ...',
              style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
            ),
          ),
          Container(
            padding: EdgeInsets.all(180),
            child: LoadingIndicator(
              indicatorType: Indicator.ballScaleRipple,
              color: Constant.colorLogoBack,
            ),
          ),
        ],
      );
    } else {
      List<dynamic> posts = this._posts[label];
      return Container(
        margin: EdgeInsets.only(
            top: Constant.containerMarginTop,
            right: Constant.containerMarginRight,
            bottom: Constant.containerMarginBottom,
            left: Constant.containerMarginLeft),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 15),
              child: Text(
                'Se encontraron ' + posts.length.toString() + ' resultados',
                style:
                    TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: posts.map((e) => this._getItem(label, e)).toList(),
                ),
              ),
            )
          ],
        ),
      );
    }
  }

  // arma un item de la lista de respuesta de la API
  Widget _getItem(String label, e) {
    // print(e['ITEMS']);
    return Container(
      child: ConfigurableExpansionTile(
        // // ANIMACION LUEGO DE DESGLOSAR EL TITULO, AUN CON ERRORES DESCONOCIDOS
        headerExpanded: Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width,
            // padding: EdgeInsets.all(5),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // label == 'Todos los contenidos'
                      //     ? Container(
                      //         width: double.infinity,
                      //         child: Text(
                      //           e['etiqueta'],
                      //           style: TextStyle(),
                      //         ),
                      //       )
                      //     : Container(),
                      Text(
                        e['Titulo'],
                        style: TextStyle(
                            color: Constant.primaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 7),
                      Container(
                          // width: double.infinity,
                          alignment: Alignment.centerLeft,
                          child: Text(e['Decreto_legislativo'],
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 11))),
                      label == 'Todos los contenidos'
                          ? Container(
                              width: double.infinity,
                              child: Text(
                                  // 'Código de Norma ' +
                                  //     e['id_norma'] +
                                  //     ' ' +
                                  e['norma'],
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 12)),
                            )
                          : Container(),
                      Container(
                        child: Divider(
                          thickness: 1,
                        ),
                      )
                    ],
                  ),
                ),
                // Icon(Icons.arrow_drop_up, color: Constant.colorLogoBack),
                Icon(FontAwesomeIcons.featherAlt,
                    color: Constant.colorLogoBack, size: 20),
              ],
            ), // Text("A Header"),
          ),
        ),
        header: Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width,
            // padding:EdgeInsets.all(5),
            // width: MediaQuery.of(context).size.width * 0.93,
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // label == 'Todos los contenidos'
                      //     ? Container(
                      //         width: double.infinity,
                      //         child: Text(
                      //           e['etiqueta'],
                      //           style: TextStyle(),
                      //         ),
                      //       )
                      //     : Container(),
                      Text(
                        e['Titulo'],
                        style: TextStyle(
                            color: Constant.primaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 7),
                      Container(
                          // width: double.infinity,
                          alignment: Alignment.centerLeft,
                          child: Text(e['Decreto_legislativo'],
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 11))),
                      label == 'Todos los contenidos'
                          ? Container(
                              width: double.infinity,
                              child: Text(
                                  // 'Código de Norma ' +
                                  //     e['id_norma'] +
                                  //     ' ' +
                                  e['norma'],
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 12)),
                            )
                          : Container(),
                      Container(
                        child: Divider(
                          thickness: 1,
                        ),
                      )
                    ],
                  ),
                ),
                Icon(Icons.arrow_drop_down),
              ],
            ), // Text("A Header"),
          ),
        ),
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(10),
            // color: Color.fromRGBO(200, 200, 200, 0.15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(e['Contenido']),
                this._getContenidoItems(e['Contenido'], e['ITEMS'], context),
              ],
            ),
          ),
        ],
      ),
    );
    // ===================================================
  }

  /* End Widgets */
  // Widget _getItem2(String label, e) {
  //   return InkWell(
  //     child: Container(
  //       child: Column(
  //         children: [
  //           label == 'Todos los contenidos'
  //               ? Container(
  //                   width: double.infinity,
  //                   margin: EdgeInsets.only(bottom: 3),
  //                   child: Text(
  //                     e['etiqueta'],
  //                     style: TextStyle(),
  //                   ),
  //                 )
  //               : Container(),
  //           Container(
  //             width: double.infinity,
  //             margin: EdgeInsets.only(bottom: 5),
  //             child: Text(
  //               e['Titulo'],
  //               style: TextStyle(
  //                   color: Constant.primaryColor, fontWeight: FontWeight.bold),
  //             ),
  //           ),
  //           Container(
  //               width: double.infinity,
  //               child: Text(e['Decreto_legislativo'],
  //                   style: TextStyle(color: Colors.grey, fontSize: 11))),
  //           label == 'Todos los contenidos'
  //               ? Container(
  //                   width: double.infinity,
  //                   child: Text(
  //                       'Código de Norma ' + e['id_norma'] + ' ' + e['norma'],
  //                       style: TextStyle(color: Colors.black54, fontSize: 12)),
  //                 )
  //               : Container(),
  //           Container(
  //             child: Divider(
  //               thickness: 1,
  //             ),
  //           )
  //         ],
  //       ),
  //     ),
  //     onTap: () {
  //       print('PRESIONADO SOLO UNA FILA');
  //       print(e['Contenido']);
  //       print(e['id_norma']);
  //     },
  //   );
  // }

  // arma el contenido del articulo
  Widget _getArticuloContenido(String contenido) {
    return Container(
      child: SelectableText.rich(
        TextSpan(
          children: [
            TextSpan(text: contenido),
          ],
        ),
        // toolbarOptions: new ToolbarOptions(copy: true, selectAll: false),
      ),
    );
  }

  // arma el contenido de los items, con decodificacion de texto plano
  Widget _getContenidoItems(
      String contenido, List itemsDentro, BuildContext context) {
    int cantidad = itemsDentro.length;
    String contTemp = contenido;
    contTemp = contTemp.replaceAll("\r\n\r\n", "\n");
    // print("cantidad -> : " + cantidad.toString());

    if (cantidad == 0) {
      print("================================================================");
      return this._getArticuloContenido(contenido);
    } else {
      List<Widget> widgets = [];

      for (var i = 0; i < cantidad; i++) {
        // print('proceso: '+(i+1).toString());
        // print("contenido todal: "+contTemp);
        // print('se busca: '+itemsDentro[i]['titulo'].toString() + " en: "+contTemp);
        // print('se busca: '+itemsDentro[i]['titulo'].toString() + " en: contenido");
        List split = contTemp.split(itemsDentro[i]['titulo']);
        // print(split);

        widgets.add(Text(split[0]));
        //modal
        // widgets.add(Text (itemsDentro[i]['titulo'],style: TextStyle(color:Colors.red),));
        widgets.add(_insertModal(
            itemsDentro[i]['titulo'], itemsDentro[i]['contenido'], context));
        //endmodal
        String temp = '';
        for (var j = 0; j < split.length; j++) {
          // 0  , 1  , 2   (3)
          if (j > 0) {
            if (j == split.length - 1) {
              temp = temp + split[j];
            } else {
              temp = temp + split[j] + itemsDentro[i]['titulo'];
            }
          }
        }
        if ((i + 1 == cantidad) && split.length == 2) {
          // print("IF FINAL: ");
          // print(split);
          widgets.add(Text(split[1]));
        }
        contTemp = temp;
      }
      // print("================================================================");
      return Container(
        // padding: EdgeInsets.only(left: 10, right: 10),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: widgets),
      );
    }
  }

  // inserta un modal a la funcion anterior
  Widget _insertModal(String titulo, String cont, BuildContext context) {
    return InkWell(
      child: Text(
        titulo,
        style: TextStyle(
          color: Color(0xFFA44702),
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {
        print('presionado :' + cont);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Row(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Constant.colorLogoBack,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  Center(
                      child: Text(titulo,
                          style: TextStyle(
                            color: Color.fromRGBO(135, 24, 48, 1),
                          ))),
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Center(
                      child: Text(cont),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: Text('Ok',
                      style: TextStyle(color: Constant.colorLogoBack)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }
}
