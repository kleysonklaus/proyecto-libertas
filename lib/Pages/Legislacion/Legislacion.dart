import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Fonts/libertas_icons.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
import 'package:libertas6/Pages/Legislacion/LegislacionDetalle.dart';
import 'package:libertas6/Pages/Legislacion/TituloEspecial.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

// import 'LegislacionDetalle.dart';

class Legislacion extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LegislacionState();
  }
}

class _LegislacionState extends State<Legislacion>
    with TickerProviderStateMixin {
  List<dynamic> _posts;
  TabController _tabController;

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getAppMenuTopProfile(),
      // WidgetHelper.getBuscador("legislacion", context),
      this._getTabs(),
      this._getTabContent()
    ];
    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    this._setPosts();
  }

  // obtiene datos del json almacenado
  void _setPosts() async {
    RequestHelper.getListFromConfig('lista_legislacion.json').then((response) {
      setState(() {
        this._posts = response;
      });
    });
  }

  // arma los contenidos en tabs de legislacion
  Widget _getTabs() {
    return Container(
      child: TabBar(
        controller: _tabController,
        indicatorSize: TabBarIndicatorSize.label,
        labelColor: Constant.primaryColor,
        unselectedLabelColor: Constant.disabledColor,
        isScrollable: true,
        onTap: (value) {
          //print(value);
        },
        tabs: [
          Container(
            width: 60,
            alignment: Alignment.center,
            child: Text('Códigos'),
          ),
          Tab(text: 'Legislación por materias')
        ],
      ),
    );
  }

  // obtiene el texto titulo para un item
  Widget _getWidgetItemTitle(String title) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 10.0),
      child: Text(
        title,
        style: TextStyle(
          color: Constant.primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 17,
          //fontFamily: Design.itemTitleFontFamily
        ),
        textAlign: TextAlign.left,
      ),
    );
  }

  // obtiene el contenido para un item
  Widget _getWidgetItemContent(Map<String, dynamic> content) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: InkWell(
        onTap: () {
          // String _url = content["url"];
          print(content["url"]);
          // String _numNorma = content['numNorma'];
          if (content["url"] != "/get_codigopenalprocesal/") {
            pushNewScreen(
              context,
              screen: LegislacionDetalle(
                  url: content["url"], numNorma: content['numNorma']),
              withNavBar: true, // OPTIONAL VALUE. True by default.
              pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          } else {
            print('codigo procesal penal ===========');
            pushNewScreen(
              context,
              screen: TituloEspecial(
                  id: content['numNorma'],
                  url: content["url"],
                  nombre: content["label"]),
              withNavBar: true, // OPTIONAL VALUE. True by default.
              pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          }
        },
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 10),
              // child: new Icon
              // (
              //     Icons.access_alarms,
              //     color: Color(0xFFD07912),
              // )
              // child: new ImageIcon
              // (
              //     AssetImage('assets/icons/png/pdf.png'),
              //     color: Color(0xFFD07912),
              //     size: 14,
              // ),
              child: Icon(myIcons[content['icon']],
                  color: Color(0xFFD07912), size: 16),
            ),
            Expanded(
              child: Text(
                content['label'],
                style: TextStyle(color: Constant.contentColor, fontSize: 16),
                textAlign: TextAlign.justify,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // obtiene toda la lista de contenido de la pantalla
  Widget _getCodigosLista() {
    return Container(
      padding: EdgeInsets.only(
        top: 20,
        left: Constant.containerMarginLeft,
        right: Constant.containerMarginRight,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: this._posts.map((e) {
          return Container(
            margin: EdgeInsets.only(bottom: 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                this._getWidgetItemTitle(e['titulo']),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: e['items'].map<Widget>((f) {
                    return this._getWidgetItemContent(f);
                  }).toList(),
                ),
              ],
            ),
          );
        }).toList(),
      ),
    );
    // return SingleChildScrollView(
    //   padding: EdgeInsets.only(
    //       top: 20,
    //       left: Constant.containerMarginLeft,
    //       right: Constant.containerMarginRight),
    //   child: Column(
    //     children: this._posts.map<Widget>((e) {
    //       return Container(
    //         margin: EdgeInsets.only(bottom: 15),
    //         child: Column(
    //           mainAxisSize: MainAxisSize.min,
    //           children: [
    //             this._getWidgetItemTitle(e['titulo']),
    //             Column(
    //               mainAxisSize: MainAxisSize.min,
    //               children: e['items'].map<Widget>((f) {
    //                 return this._getWidgetItemContent(f);
    //               }).toList(),
    //             )
    //           ],
    //         ),
    //       );
    //     }).toList(),
    //   ),
    // );
  }

  // arma el tab contenido (no se usa)
  Widget _getTabContent() {
    return this._posts == null
        ? WidgetHelper.getWidgetLoader()
        : Expanded(
            child: ListView(
              children: [
                this._getCodigosLista(),
              ],
            ),
          );
    //================
    // return Container(
    //   child: this._getCodigosLista(),
    // );
    //==============================
    // new Container(
    //   child: Expanded(
    //     //child:
    //     child: TabBarView(
    //       controller: _tabController,
    //       children: [
    //         this._getCodigosLista(),
    //         // Icon(Icons.directions_transit),
    //       ],
    //     ),
    //   ),
    // );
  }
}
