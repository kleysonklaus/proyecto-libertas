// import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
// import 'package:libertas6/Modals/ConcordanciaModal.dart';
// import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class Titulo extends StatefulWidget {
  final String id;
  final String nombre;

  const Titulo({Key key, this.id, this.nombre}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _TituloState();
  }
}

class _TituloState extends State<Titulo> {
  List<dynamic> _posts;

  @override
  void initState() {
    super.initState();
    this._setPosts();
  }

  // obtiene datos para cierto contenido de legislacionDetalle.dart
  void _setPosts() async {
    RequestHelper.getListFromAPI('/get_titulocontenido/' + this.widget.id)
        .then((response) {
      setState(() {
        this._posts = response;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      this.getContenido(),
    ];
    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  // arma widget de contenido para el item
  Widget getContenido() {
    return Container(
      width: MediaQuery.of(context).size.width - 20,
      height: MediaQuery.of(context).size.height - 100,
      margin: EdgeInsets.only(
        top: 0,
        left: Constant.containerMarginLeft,
        right: Constant.containerMarginRight,
      ),
      child: Column(
        // shrinkWrap: true,
        // mainAxisSize: MainAxisSize.min,
        children: [
          this._getTitulo(),
          this._getArticulos(),
        ],
      ),
    );
  }

  // obtiene el titulo del item
  Widget _getTitulo() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(
        bottom: 15,
        top: 15,
      ),
      child: Text(
        this.widget.nombre,
        style: TextStyle(
          color: Constant.primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  // obtiene la lista de articulos del item
  Widget _getArticulos() {
    return this._posts == null
        ? WidgetHelper.getWidgetLoader()
        : Expanded(
            child: ListView(
              shrinkWrap: true,
              // mainAxisSize: MainAxisSize.min,

              children: this._posts.map(
                (e) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      this._getArticuloTitulo(e['Titulo']),
                      this._getContenidoItems(
                          e['Contenido'], e['ITEMS'], context),
                      // this._getArticuloContenido(e['Contenido']),
                      // this._getITEMS(e['ITEMS'], context),
                      Divider(height: 25, thickness: 2)
                    ],
                  );
                },
              ).toList(),
            ),
          );
  }

  // obtiene los titulos de cada articulo del item
  Widget _getArticuloTitulo(String titulo) {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      alignment: Alignment.centerLeft,
      child: Text(
        titulo,
        style: TextStyle(
            color: Constant.primaryColor, fontWeight: FontWeight.bold),
      ),
    );
  }

  // obtiene el contenido del articulo
  Widget _getArticuloContenido(String contenido) {
    return Container(
      child: SelectableText.rich(
        TextSpan(
          children: [
            TextSpan(text: contenido),
          ],
        ),
        // toolbarOptions: new ToolbarOptions(copy: true, selectAll: false),
      ),
    );
  }

  // Widget _getITEMS(List itemsDentro, BuildContext context) {
  //   return itemsDentro == null
  //       ? Container()
  //       : Container(
  //           child: Row(
  //             // mainAxisAlignment: MainAxisAlignment.center,
  //             children: itemsDentro.map((e) {
  //               return Container(
  //                 margin: EdgeInsets.only(right: 10),
  //                 child: InkWell(
  //                     child: Text(
  //                       e['titulo'],
  //                       style: TextStyle(
  //                         color: Color(0xFFA44702),
  //                         fontSize: 15,
  //                         // fontWeight: FontWeight.bold,
  //                       ),
  //                     ),
  //                     onTap: () {
  //                       print('presionado :' + e['titulo']);
  //                       showDialog(
  //                         context: context,
  //                         builder: (BuildContext context) {
  //                           return AlertDialog(
  //                             title: Row(
  //                               mainAxisAlignment: MainAxisAlignment.start,
  //                               children: [
  //                                 IconButton(
  //                                   icon: Icon(
  //                                     Icons.arrow_back,
  //                                     color: Constant.colorLogoBack,
  //                                   ),
  //                                   onPressed: () {
  //                                     Navigator.of(context).pop();
  //                                   },
  //                                 ),
  //                                 Text(
  //                                   e['titulo'],
  //                                   style: TextStyle(
  //                                     color: Color.fromRGBO(135, 24, 48, 1),
  //                                   ),
  //                                 ),
  //                               ],
  //                             ),
  //                             content: SingleChildScrollView(
  //                               child: ListBody(
  //                                 children: <Widget>[
  //                                   Center(
  //                                     child: Text(e['contenido']),
  //                                   ),
  //                                 ],
  //                               ),
  //                             ),
  //                           );
  //                         },
  //                       );
  //                     }),
  //               );
  //             }).toList(),
  //           ),
  //         );
  // }

  // arma los widgets items, con decodificacion de texto plano
  Widget _getContenidoItems(
      String contenido, List itemsDentro, BuildContext context) {
    int cantidad = itemsDentro.length;
    String contTemp = contenido;
    contTemp = contTemp.replaceAll("\r\n\r\n", "\n");
    print("cantidad -> : " + cantidad.toString());

    if (cantidad == 0) {
      print("================================================================");
      return this._getArticuloContenido(contenido);
    } else {
      List<Widget> widgets = [];

      for (var i = 0; i < cantidad; i++) {
        // print('proceso: '+(i+1).toString());
        // print("contenido todal: "+contTemp);
        // print('se busca: '+itemsDentro[i]['titulo'].toString() + " en: "+contTemp);
        // print('se busca: '+itemsDentro[i]['titulo'].toString() + " en: contenido");
        List split = contTemp.split(itemsDentro[i]['titulo']);
        // print(split);

        widgets.add(Text(split[0]));
        //modal
        // widgets.add(Text (itemsDentro[i]['titulo'],style: TextStyle(color:Colors.red),));
        widgets.add(_insertModal(
            itemsDentro[i]['titulo'], itemsDentro[i]['contenido'], context));
        //endmodal
        String temp = '';
        for (var j = 0; j < split.length; j++) {
          // 0  , 1  , 2   (3)
          if (j > 0) {
            if (j == split.length - 1) {
              temp = temp + split[j];
            } else {
              temp = temp + split[j] + itemsDentro[i]['titulo'];
            }
          }
        }
        if ((i + 1 == cantidad) && split.length == 2) {
          // print("IF FINAL: ");
          // print(split);
          widgets.add(Text(split[1]));
        }
        contTemp = temp;
      }
      // print("================================================================");
      return Container(
        // padding: EdgeInsets.only(left: 10, right: 10),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: widgets),
      );
    }
  }

  // inserta un modal a la funcion anterior
  Widget _insertModal(String titulo, String cont, BuildContext context) {
    return InkWell(
      child: Text(
        titulo,
        style: TextStyle(
          color: Color(0xFFA44702),
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {
        print('presionado :' + cont);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Row(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Constant.colorLogoBack,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  Center(
                      child: Text(titulo,
                          style: TextStyle(
                            color: Color.fromRGBO(135, 24, 48, 1),
                          ))),
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Center(
                      child: Text(cont),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: Text('Ok',
                      style: TextStyle(color: Constant.colorLogoBack)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }
}
