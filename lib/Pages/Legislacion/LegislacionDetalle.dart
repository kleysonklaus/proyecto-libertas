import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';

import 'package:libertas6/Constant.dart';
import 'package:libertas6/Pages/Legislacion/Titulo.dart';

class LegislacionDetalle extends StatefulWidget {
  final String url;
  final String numNorma;

  const LegislacionDetalle({Key key, this.url, this.numNorma})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LegislacionDetalleState();
  }
}

class _LegislacionDetalleState extends State<LegislacionDetalle>
    with TickerProviderStateMixin {
  List<dynamic> _posts;
  TabController _tabController;
  double cp = 5;
  // TextEditingController _controllArticle;

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getAppMenuTopProfile(cambio: true, urlTemp: widget.numNorma),
      this._getTabContent(),
    ];
    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  @override
  void initState() {
    super.initState();
    print('DENTRO DEL INIT: ' + widget.url);
    _tabController = new TabController(length: 2, vsync: this);
    this._setPosts();
  }

  // respuesta de la aPI contenido
  void _setPosts() async {
    RequestHelper.getListFromAPI(this.widget.url).then((response) {
      setState(() {
        this._posts = response;
      });
    });
  }

  /* Widgets */
  // Widget _getTabs() {
  //   return Container(
  //       child: TabBar(
  //           controller: this._tabController,
  //           indicatorSize: TabBarIndicatorSize.label,
  //           labelColor: Constant.primaryColor,
  //           unselectedLabelColor: Constant.disabledColor,
  //           isScrollable: true,
  //           onTap: (value) {
  //             // print(value);
  //           },
  //           tabs: [
  //         Container(
  //           width: 60,
  //           alignment: Alignment.center,
  //           child: Text('Indice'),
  //         ),
  //         Tab(text: 'Ir a')
  //       ]));
  // }

  // arma los tab contenido
  Widget _getTabContent() {
    return this._posts == null
        ? WidgetHelper.getWidgetLoader()
        // : _getCodigosDetalleLista();
        : Expanded(
            child: ListView(
              shrinkWrap: true,
              children: [
                _getCodigosDetalleLista(),
              ],
            ),
          );
    // return Container(
    //   //child: Expanded(
    //   // child: TabBarView(
    //   //   controller: _tabController, //### ELIMINANDO INDICE- IR A
    //   //   children: [
    //   //     this._getCodigosDetalleLista(),
    //   //     Icon(Icons.directions_transit),
    //   //   ],
    //   // ),
    //   child: this._getCodigosDetalleLista(),
    //   //),
    // );
  }

  // arma la lista de contenido
  Widget _getCodigosDetalleLista() {
    return Container(
      padding: EdgeInsets.only(top: 15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: this._posts.map((e) {
          return this._getLegislacionExpansionTile(e, 1, cp + 10);
        }).toList(),
      ),
    );
    // return Container(
    //   margin: EdgeInsets.only(top: 14),
    //   height: MediaQuery.of(context).size.height - 178,
    //   // height: MediaQuery.of(context).size.height -
    //   //     564, // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //   child: ListView(
    //       children: this._posts.map((e) {
    //     // return this._getLegislacionExpansionTile(e, 1);
    //     return this._getLegislacionExpansionTile(e, 1, cp + 10);
    //   }).toList()),
    // );
  }

  // arma el wirdget expandible para cada contenido
  Widget _getLegislacionExpansionTile(
      Map<String, dynamic> item, int level, double cp) {
    // double childrenPadding = 18;
    TextStyle textStyle = TextStyle(fontSize: (16 - level).toDouble());

    List<Widget> wChildren = new List();
    String childrenKey = 'children_' + level.toString();

    if (item.containsKey(childrenKey)) {
      List children = item[childrenKey];
      children.forEach((element) {
        // wChildren.add(this._getLegislacionExpansionTile(element, level + 1));
        wChildren
            .add(this._getLegislacionExpansionTile(element, level + 1, cp + 0));
      });
    }

    if (wChildren.length == 0) {
      String titulo = item['titulo_' + level.toString()];

      return Container(
        margin: EdgeInsets.only(left: 15, top: 5, bottom: 5),
        width: double.infinity,
        child: InkWell(
          child: Text(titulo, style: textStyle),
          onTap: () {
            pushNewScreen(
              context,
              screen: Titulo(
                  id: item['id_titulo'].toString(),
                  // .toLowerCase(),
                  nombre: titulo),
              withNavBar: true, // OPTIONAL VALUE. True by default.
              pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
        ),
      );
    } else {
      return Container(
        // padding: EdgeInsets.only(top: 15, bottom: 5),
        child: ConfigurableExpansionTile(
          // // ANIMACION LUEGO DE DESGLOSAR EL TITULO, AUN CON ERRORES DESCONOCIDOS
          headerExpanded: Expanded(
            child: Container(
              padding: EdgeInsets.only(left: cp, top: 6, bottom: 6, right: 10),
              width: MediaQuery.of(context).size.width * 0.93,
              // color: Color.fromRGBO(200, 200, 200, 1),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      item['titulo_' + level.toString()].toString(),
                      // .toLowerCase(),
                      style: TextStyle(color: Constant.colorLogoBack),
                    ),
                  ),
                  // Icon(Icons.arrow_drop_up, color: Constant.colorLogoBack),
                  Icon(FontAwesomeIcons.featherAlt,
                      color: Constant.colorLogoBack, size: 20),
                ],
              ), // Text("A Header"),
            ),
          ),
          header: Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: cp, top: 5, bottom: 5, right: 10),
              // width: MediaQuery.of(context).size.width * 0.93,
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      item['titulo_' + level.toString()].toString(),
                      // .toLowerCase(),
                      style: textStyle,
                    ),
                  ),
                  Icon(Icons.arrow_drop_down),
                ],
              ), // Text("A Header"),
            ),
          ),
          children: [
            Container(
              padding: EdgeInsets.only(left: cp),
              width: MediaQuery.of(context).size.width * 0.93,
              // color: Color.fromRGBO(200, 200, 200, 0.15),
              child: Column(
                children: wChildren,
                // Padding: new EdgeInsets.only(left: childrenPadding),
              ),
            ),
          ],
        ),
      );
    }
  }
}
