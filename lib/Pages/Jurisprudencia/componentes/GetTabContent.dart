import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Fonts/libertas_icons.dart';
import 'package:libertas6/Pages/Jurisprudencia/componentes/JurisprudenciaDetalle.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class GetTabContent extends StatefulWidget {
  final List<dynamic> post;
  GetTabContent({Key key, this.post}) : super(key: key);

  @override
  _GetTabContentState createState() => _GetTabContentState();
}

class _GetTabContentState extends State<GetTabContent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _getCodigosDesplegable(),
    );
  }

  // arma el widget desplegable de cada contenido
  Widget _getCodigosDesplegable() {
    return ListView(
      shrinkWrap: true,
      children: widget.post.map((e) {
        return e['hoja']
            ? InkWell(
                child: Container(
                  // padding: EdgeInsets.only(top: 20, bottom: 15),
                  margin:
                      EdgeInsets.only(left: 25, right: 15, top: 15, bottom: 5),
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: [
                      Icon(myIcons[e['icon']],
                          color: Color(0xFFD07912), size: 20),
                      SizedBox(
                        width: 8,
                      ),
                      Expanded(
                        child: Text(
                          e['label'],
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      )
                    ],
                  ), // Text("A Header"),
                ),
                onTap: () {
                  print(e['id']);
                  pushNewScreen(
                    context,
                    screen: JurisprudenciaDetalle(
                      url: e["url"],
                      id: e['id'],
                      label: e['label'],
                    ),
                    withNavBar: true,
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  );
                },
              )
            : Container(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                margin: EdgeInsets.only(left: 15, right: 15),
                child: ConfigurableExpansionTile(
                  initiallyExpanded: true,
                  headerExpanded: Expanded(
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 10, top: 5, bottom: 5, right: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: [
                          Icon(myIcons[e['icon']],
                              color: Color(0xFFD07912), size: 20),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Text(
                              e['label'],
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Icon(FontAwesomeIcons.gavel,
                              color: Constant.colorLogoBack, size: 20),
                        ],
                      ), // Text("A Header"),
                    ),
                  ),
                  header: Expanded(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(
                          left: 10, top: 5, bottom: 5, right: 10),
                      // width: MediaQuery.of(context).size.width * 0.93,
                      child: Row(
                        children: [
                          Icon(myIcons[e['icon']],
                              color: Color(0xFFD07912), size: 20),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Text(
                              e['label'],
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Icon(Icons.arrow_drop_down),
                        ],
                      ), // Text("A Header"),
                    ),
                  ),
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 15),
                      margin: EdgeInsets.only(top: 5, bottom: 5),
                      // color: Color.fromRGBO(200, 200, 200, 0.15),
                      child: Column(
                        children: e['sub_categorias'].map<Widget>((hijo) {
                          return InkWell(
                            child: Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              padding: EdgeInsets.all(6),
                              alignment: Alignment.centerLeft,
                              child: Text(hijo['label']),
                            ),
                            onTap: () {
                              print(hijo['id']);
                              pushNewScreen(
                                context,
                                screen: JurisprudenciaDetalle(
                                    url: e["url"],
                                    id: hijo['id'],
                                    label: hijo['label']),
                                withNavBar:
                                    true, // OPTIONAL VALUE. True by default.
                                pageTransitionAnimation:
                                    PageTransitionAnimation.cupertino,
                              );
                            },
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
              );
      }).toList(),
    );
  }
}
