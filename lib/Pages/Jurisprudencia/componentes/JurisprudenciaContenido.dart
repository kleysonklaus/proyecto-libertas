import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';

class JurisprudenciaContenido extends StatelessWidget {
  final String titulo;
  final String sumilla;
  final String contenido;
  const JurisprudenciaContenido(
      {Key key, this.titulo, this.sumilla, this.contenido})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getAppMenuTopProfile(),
      this._getAllInformation(),
    ];
    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  // arma la lista de contenidos general recibido como parametro
  Widget _getAllInformation() {
    return Expanded(
      child: ListView(
        children: [
          this._getTitulo(this.titulo),
          this._getSumilla(this.sumilla),
          this._getContenido(this.contenido),
        ],
      ),
    );
  }

  // arma el titulo del contenido
  Widget _getTitulo(String titu) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Text(
        titu,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 17,
            color: Constant.colorLogoBack),
      ),
    );
  }

  // arma la sumilla del contenido
  Widget _getSumilla(String sum) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            'sumilla',
            style: TextStyle(
                fontStyle: FontStyle.italic,
                fontSize: 17,
                color: Color(0xFFD07912)),
          ),
          Text(
            sum,
            style: TextStyle(
                // fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
                fontSize: 17,
                color: Constant.colorLogoBack),
          ),
        ],
      ),
    );
  }

  // arma el contenido
  Widget _getContenido(String cont) {
    print(cont);
    return Container(
      margin: EdgeInsets.all(10),
      child: Text(cont),
    );
  }
}
