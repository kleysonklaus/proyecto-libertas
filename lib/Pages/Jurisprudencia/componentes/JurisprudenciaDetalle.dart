import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
import 'package:libertas6/Pages/Jurisprudencia/componentes/JurisprudenciaContenido.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class JurisprudenciaDetalle extends StatefulWidget {
  final String url;
  final String id;
  final String label;
  JurisprudenciaDetalle({Key key, this.url, this.id, this.label})
      : super(key: key);

  @override
  _JurisprudenciaDetalleState createState() => _JurisprudenciaDetalleState();
}

class _JurisprudenciaDetalleState extends State<JurisprudenciaDetalle> {
  List<dynamic> _posts;
  bool existeDatos = false;
  @override
  void initState() {
    super.initState();
    this._setPosts();
  }

  void _setPosts() async {
    RequestHelper.getListFromAPI(this.widget.url + this.widget.id)
        .then((response) {
      setState(() {
        this._posts = response;
        existeDatos = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getAppMenuTopProfile(),
      this._getContent(),
    ];
    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  // arma el tab contenido general
  Widget _getContent() {
    return !existeDatos
        ? WidgetHelper.getWidgetLoader()
        : Expanded(
            child: ListView(
              shrinkWrap: true,
              children: _posts.map((e) {
                return InkWell(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      // this._getLabel(widget.label),
                      this._getLitleTitulo(e['Titulo']),
                      // this._getTitulo(e['Titulo']),
                      this._getLitleSumilla(e['Sumilla']),
                      // this._getSumilla(e['Sumilla']),
                      // this._getContenido(e['Contenido']),
                      Divider(
                        // height: 25,
                        thickness: 2,
                      )
                    ],
                  ),
                  onTap: () {
                    print('asd');
                    pushNewScreen(
                      context,
                      screen: JurisprudenciaContenido(
                        titulo: e['Titulo'],
                        sumilla: e['Sumilla'],
                        contenido: e['Contenido'],
                      ),
                      withNavBar: true, // OPTIONAL VALUE. True by default.
                      pageTransitionAnimation:
                          PageTransitionAnimation.cupertino,
                    );
                  },
                );
              }).toList(),
            ),
          );
  }

  // Widget _getLabel(String lab) {
  //   return Container(
  //     alignment: Alignment.centerLeft,
  //     margin: EdgeInsets.all(10),
  //     child: Text(
  //       lab,
  //       style: TextStyle(
  //           // fontWeight: FontWeight.bold,
  //           fontSize: 16,
  //           color: Constant.colorLogoBack),
  //     ),
  //   );
  // }

  // Widget _getTitulo(String titulo) {
  //   return Container(
  //     margin: EdgeInsets.all(10),
  //     child: Text(
  //       titulo,
  //       style: TextStyle(
  //           fontWeight: FontWeight.bold,
  //           fontSize: 17,
  //           color: Constant.colorLogoBack),
  //     ),
  //   );
  // }

  // arma el titulo del contenido
  Widget _getLitleTitulo(String titulo) {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 13),
      height: 40,
      child: Text(
        _editTitulo(titulo),
        style: TextStyle(
            // fontWeight: FontWeight.bold,
            fontSize: 17,
            color: Constant.colorLogoBack),
      ),
    );
  }

  // Widget _getContenido(String contenido) {
  //   return Container(
  //     margin: EdgeInsets.all(10),
  //     child: Text(contenido),
  //   );
  // }

  // Widget _getSumilla(String sumilla) {
  //   return Container(
  //     margin: EdgeInsets.all(10),
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.stretch,
  //       children: [
  //         Text(
  //           'Sumilla',
  //           style: TextStyle(
  //               fontStyle: FontStyle.italic,
  //               fontSize: 17,
  //               color: Color(0xFFD07912)),
  //         ),
  //         Text(
  //           sumilla,
  //           style: TextStyle(
  //               // fontWeight: FontWeight.bold,
  //               fontStyle: FontStyle.italic,
  //               fontSize: 17,
  //               color: Constant.colorLogoBack),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // arma la sumilla del contenido
  Widget _getLitleSumilla(String sumilla) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10),
      height: 30,
      child: Text(
        _editSumilla(sumilla),
        style: TextStyle(
          // fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
          fontSize: 13,
          color: Constant.disabledColor,
        ),
      ),
    );
  }

  // edita la sumilla para que solo se muestre una pequeña parte del mismo
  String _editSumilla(String s) {
    int long = s.length;
    if (long > 100) {
      return s.substring(1, 100) + ' ...';
    } else {
      return s + ' ...';
    }
  }

  // edita el titulo para que solo se muestre una pequeña parte del mismo
  String _editTitulo(String s) {
    int long = s.length;
    if (long > 100) {
      return s.substring(1, 100) + ' ...';
    } else {
      return s;
    }
  }
}
