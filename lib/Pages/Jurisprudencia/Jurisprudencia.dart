import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Fonts/libertas_icons.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
// import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:libertas6/Pages/Jurisprudencia/componentes/JurisprudenciaDetalle.dart';
import 'package:libertas6/Pages/Jurisprudencia/componentes/GetTabContent.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class Jurisprudencia extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _JurisprudenciaState();
  }
}

class _JurisprudenciaState extends State<Jurisprudencia>
    with TickerProviderStateMixin {
  List<dynamic> _posts;
  TabController _tabController;

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getAppMenuTopProfile(),
      // WidgetHelper.getBuscador("legislacion", context),
      this._getTabs(),
      this._getTabContent()
    ];
    return WidgetHelper.getMainContainerWithAppBar(widgets);
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    this._setPosts();
  }

  void _setPosts() async {
    RequestHelper.getListFromConfig('lista_jurisprudencia3.json')
        .then((response) {
      setState(() {
        this._posts = response;
      });
    });
  }

  // arma el tab principal de contenidos
  Widget _getTabs() {
    return Container(
      height: 50,
      child: TabBar(
        controller: _tabController,
        indicatorSize: TabBarIndicatorSize.label,
        labelColor: Constant.primaryColor,
        unselectedLabelColor: Constant.disabledColor,
        isScrollable: true,
        onTap: (value) {
          // print(value);
        },
        tabs: [
          Tab(text: 'Poder Judicial'),
          Tab(text: 'Tribunal constitucional')
        ],
      ),
    );
  }

  // arma el tab con el contenido ya obtenido de la API
  Widget _getTabContent() {
    return this._posts == null
        ? WidgetHelper.getWidgetLoader()
        : Expanded(
            child: TabBarView(
              controller: _tabController,
              children: _posts.map((todo) {
                return GetTabContent(post: todo['contenido']);
              }).toList(),
            ),
          );

    // Container(
    //     color: Colors.orange,
    //     height: MediaQuery.of(context).size.height - 250,
    //     child: TabBarView(
    //       controller: _tabController,
    //       children: [
    //         this._getCodigosLista(),
    //         Icon(Icons.directions_transit),
    //       ],
    //     ),
    //   );
  }

  // Widget _getCodigosLista() {
  //   return ListView(
  //     shrinkWrap: true,
  //     children: this
  //         ._posts
  //         .map<Widget>(
  //           (e) => this._getWidgetItemContent(e),
  //         )
  //         .toList(),
  //   );
  //   // ======================================================
  //   // if (this._posts == null) {
  //   //   return WidgetHelper.getWidgetLoader();
  //   // } else {
  //   //   // $$$ IMPRIME LOS POSTS .... LOS NOMBRES DE LOS ICONOS
  //   //   // print(this._posts);
  //   //   return Container(
  //   //     margin: EdgeInsets.only(
  //   //       top: 20,
  //   //       left: Constant.containerMarginLeft,
  //   //       right: Constant.containerMarginRight,
  //   //     ),
  //   //     child: ListView(
  //   //       // child: Column(
  //   //       // mainAxisSize: MainAxisSize.min,
  //   //       children: this
  //   //           ._posts
  //   //           .map<Widget>(
  //   //             (e) => this._getWidgetItemContent(e),
  //   //           )
  //   //           .toList(),
  //   //       // ),
  //   //     ),
  //   //   );
  //   // }
  // }

  // Widget _getWidgetItemContent(Map<String, dynamic> content) {
  //   return Container(
  //     padding: EdgeInsets.only(
  //       left: Constant.containerMarginLeft,
  //       right: Constant.containerMarginRight,
  //     ),
  //     margin: EdgeInsets.only(top: 20.0),
  //     child: InkWell(
  //       onTap: () {
  //         // pushNewScreen
  //         // (
  //         //     context,
  //         //     screen: JurisprudenciaDetalle(url: content["url"]),
  //         //     withNavBar: true, // OPTIONAL VALUE. True by default.
  //         //     pageTransitionAnimation: PageTransitionAnimation.cupertino,
  //         // );
  //       },
  //       child: Row(
  //         children: [
  //           Container(
  //               margin: EdgeInsets.only(right: 10),
  //               child: Icon(myIcons[content['icon']],
  //                   color: Color(0xFFD07912), size: 16)),
  //           Text(
  //             content['label'],
  //             style: TextStyle(color: Constant.contentColor, fontSize: 16),
  //             textAlign: TextAlign.justify,
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  // arma el contenido desplegable (no se usa)
  Widget _getCodigosDesplegable() {
    return ListView(
      shrinkWrap: true,
      children: _posts.map((e) {
        return Container(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          margin: EdgeInsets.only(left: 15, right: 15),
          child: ConfigurableExpansionTile(
            initiallyExpanded: false,
            headerExpanded: Expanded(
              child: Container(
                padding:
                    EdgeInsets.only(left: 10, top: 5, bottom: 5, right: 10),
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: [
                    Icon(myIcons[e['icon']],
                        color: Color(0xFFD07912), size: 20),
                    SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      child: Text(
                        e['label'],
                        style: TextStyle(
                          fontSize: 15,
                        ),
                      ),
                    ),
                    Icon(FontAwesomeIcons.gavel,
                        color: Constant.colorLogoBack, size: 20),
                  ],
                ), // Text("A Header"),
              ),
            ),
            header: Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding:
                    EdgeInsets.only(left: 10, top: 5, bottom: 5, right: 10),
                // width: MediaQuery.of(context).size.width * 0.93,
                child: Row(
                  children: [
                    Icon(myIcons[e['icon']],
                        color: Color(0xFFD07912), size: 20),
                    SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      child: Text(
                        e['label'],
                        style: TextStyle(
                          fontSize: 15,
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down),
                  ],
                ), // Text("A Header"),
              ),
            ),
            children: [
              Container(
                padding: EdgeInsets.only(left: 15),
                margin: EdgeInsets.only(top: 5, bottom: 5),
                color: Color.fromRGBO(200, 200, 200, 0.15),
                child: Column(
                  children: e['sub_categorias'].map<Widget>((hijo) {
                    return InkWell(
                      child: Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        padding: EdgeInsets.all(6),
                        alignment: Alignment.centerLeft,
                        child: Text(hijo['label']),
                      ),
                      onTap: () {
                        print(hijo['id']);
                        pushNewScreen(
                          context,
                          screen: JurisprudenciaDetalle(
                              url: e["url"],
                              id: hijo['id'],
                              label: hijo['label']),
                          withNavBar: true, // OPTIONAL VALUE. True by default.
                          pageTransitionAnimation:
                              PageTransitionAnimation.cupertino,
                        );
                      },
                    );
                  }).toList(),
                ),
              ),
            ],
          ),
        );
      }).toList(),
    );
  }
}
